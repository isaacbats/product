# Standard
# None

# Third party
from ariadne import ObjectType

# Local
# None

EXECUTION_VULNERABILITIES_NEW = ObjectType('ExecutionVulnerabilitiesNew')
