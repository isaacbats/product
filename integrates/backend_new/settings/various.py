from __init__ import (
    FI_DEBUG,
)

DEBUG = FI_DEBUG == 'True'
TIME_ZONE = 'America/Bogota'
