# Standard
# None

# Third party
from ariadne import ObjectType

# Local
# None

ENTITY_REPORT_SUBSCRIPTION = ObjectType('EntityReportSubscription')
