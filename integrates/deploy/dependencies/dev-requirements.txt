Django==3.1.2
GitPython==3.1.3
Jinja2==2.11.2
MarkupSafe==1.1.1
PyExcelerate==0.9.0
PyYAML==5.3.1
Pygments==2.7.1
Werkzeug==1.0.1
aniso8601==8.0.0
apipkg==1.5
asgiref==3.2.10
astroid==2.3.3
attrs==19.3.0
aws-sam-translator==1.27.0
aws-xray-sdk==2.6.0
bandit==1.6.2
boto3-stubs==1.13.25.0
boto3==1.15.16
boto==2.49.0
botocore==1.18.16
certifi==2020.6.20
cffi==1.14.3
cfn-lint==0.37.1
chardet==3.0.4
click==7.1.2
cloudmersive-virus-api-client==2.0.6
codecov==2.1.4
coverage==5.3
coveralls==2.1.2
cryptography==3.1.1
decorator==4.4.2
django-stubs==1.5.0
docker==4.3.1
docopt==0.6.2
docutils==0.16
dodgy==0.2.1
ecdsa==0.14.1
execnet==1.7.1
freezegun==0.3.15
frosted==1.4.1
future==0.18.2
gitdb==4.0.5
gunicorn==20.0.4
h11==0.9.0
httptools==0.1.1
idna==2.8
importlib-metadata==2.0.0
isort==4.3.21
jmespath==0.10.0
jsondiff==1.1.2
jsonpatch==1.26
jsonpickle==1.4.1
jsonpointer==2.0
jsonschema==3.2.0
junit-xml==1.9
lazy-object-proxy==1.4.3
lxml==4.5.2
mandrill-really-maintained==1.2.4
mccabe==0.6.1
mock==4.0.2
more-itertools==8.5.0
moto==1.3.14
mypy-boto3==1.13.25.0
mypy-extensions==0.4.3
mypy==0.780
networkx==2.5
nose==1.3.7
packaging==20.4
pbr==5.5.0
pep8-naming==0.4.1
pies==2.6.7
pluggy==0.13.1
prospector==1.2.0
py==1.9.0
pyasn1==0.4.8
pycodestyle==2.4.0
pycparser==2.20
pydocstyle==5.1.1
pyflakes==2.1.1
pylint-celery==0.3
pylint-django==2.0.12
pylint-flask==0.6
pylint-plugin-utils==0.6
pylint==2.4.4
pyotp==2.3.0
pyparsing==2.4.7
pyroma==2.6
pyrsistent==0.17.3
pytest-asyncio==0.12.0
pytest-cov==2.9.0
pytest-django==3.8.0
pytest-forked==1.3.0
pytest-rerunfailures==9.0
pytest-test-groups==1.0.3
pytest-xdist==1.32.0
pytest==5.4.1
python-dateutil==2.8.1
python-jose==3.2.0
pytz==2020.1
requests==2.24.0
requirements-detector==0.7
responses==0.12.0
rsa==4.6
s3transfer==0.3.3
setoptconf==0.2.0
six==1.15.0
smmap==3.0.4
snowballstemmer==2.0.0
sqlparse==0.4.1
sshpubkeys==3.1.0
stevedore==3.2.2
typed-ast==1.4.1
typing-extensions==3.7.4.3
urllib3==1.25.10
uvicorn==0.11.5
uvloop==0.14.0
vulture==0.24
wcwidth==0.2.5
websocket-client==0.57.0
websockets==8.1
wrapt==1.12.1
xmltodict==0.12.0
zipp==3.3.0
