# Standard library
from typing import (
    List,
    NamedTuple,
)

# Third party libraries
from aioextensions import (
    collect,
    run,
)
from async_lru import alru_cache
from backend.domain import (
    project as group_domain,
)

# Local libraries
from analytics import (
    utils,
)
from analytics.colors import (
    RISK,
)

Status = NamedTuple('Status', [
    ('group_name', str),
    ('closed_vulnerabilities', int),
    ('open_vulnerabilities', int),
])


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(group: str) -> Status:
    item = await group_domain.get_attributes(group, [
        'open_vulnerabilities',
        'closed_vulnerabilities',
    ])

    return Status(
        group_name=group.lower(),
        open_vulnerabilities=item.get('open_vulnerabilities', 0),
        closed_vulnerabilities=item.get('closed_vulnerabilities', 0),
    )


async def get_data_many_groups(groups: List[str]) -> List[Status]:
    groups_data = await collect(map(get_data_one_group, groups))

    return sorted(
        groups_data,
        key=lambda x: (
            x.closed_vulnerabilities
            / (x.closed_vulnerabilities + x.open_vulnerabilities)
            if (x.closed_vulnerabilities + x.open_vulnerabilities) > 0
            else 0
        )
    )


def format_data(data: List[Status]) -> dict:
    return dict(
        data=dict(
            columns=[
                ['Closed'] + [group.closed_vulnerabilities for group in data],
                ['Open'] + [group.open_vulnerabilities for group in data],
            ],
            colors={
                'Closed': RISK.more_passive,
                'Open': RISK.more_agressive,
            },
            type='bar',
            groups=[
                ['Closed', 'Open'],
            ],
            order=None,
            stack=dict(
                normalize=True,
            )
        ),
        legend=dict(
            position='bottom',
        ),
        axis=dict(
            x=dict(
                categories=[group.group_name for group in data],
                type='category',
                tick=dict(
                    rotate=utils.TICK_ROTATION,
                    multiline=False
                )
            ),
        ),
    )


async def generate_all():
    async for org_id, org_name, _ in (
        utils.iterate_organizations_and_groups()
    ):
        for portfolio, groups in await utils.get_portfolios_groups(org_name):
            utils.json_dump(
                document=format_data(
                    data=await get_data_many_groups(groups),
                ),
                entity='portfolio',
                subject=f'{org_id}PORTFOLIO#{portfolio}',
            )


if __name__ == '__main__':
    run(generate_all())
