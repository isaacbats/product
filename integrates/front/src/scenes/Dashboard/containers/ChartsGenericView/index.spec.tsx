import { ChartsGenericView } from "scenes/Dashboard/containers/ChartsGenericView";

describe("ChartsGenericView", () => {

  it("should return an function", () => {
    expect(typeof (ChartsGenericView))
      .toEqual("function");
  });

});
