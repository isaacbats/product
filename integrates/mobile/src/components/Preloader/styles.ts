import { StyleSheet } from "react-native";

export const styles: Dictionary = StyleSheet.create({
  container: {
    alignItems: "center",
  },
  loadingGif: {
    height: 70,
    resizeMode: "stretch",
    width: 70,
  },
});
