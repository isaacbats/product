import { StyleSheet } from "react-native";

export const styles: Dictionary = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    flexDirection: "row",
    marginBottom: 10,
  },
  disabled: {
    backgroundColor: "#F4F5F7",
  },
  icon: {
    height: 26,
    marginLeft: 10,
    width: 26,
  },
  label: {
    color: "#172B4D",
    fontSize: 14,
    marginHorizontal: 10,
    marginVertical: 9,
    textAlign: "center",
  },
});
