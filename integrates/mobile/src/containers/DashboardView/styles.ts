import { StyleSheet } from "react-native";

export const styles: Dictionary = StyleSheet.create({
  bottom: {
    alignItems: "center",
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: 5,
  },
  container: {
    flex: 1,
    paddingTop: 10,
  },
  dotsContainer: {
    flexDirection: "row",
    justifyContent: "center",
  },
  scrollContainer: {
    flexGrow: 0,
  },
});
