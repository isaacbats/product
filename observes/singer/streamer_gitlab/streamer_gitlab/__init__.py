from .cli import parse_args
__version__ = '0.1.0'

if __name__ == "__main__":
    parse_args()
