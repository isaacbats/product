"""Setup package."""

from distutils.core import setup

setup(
    name='utilities',
    version='1.0.0',
    description='Forces Exploits Utilities',
    author='Fluid Attacks',
    author_email='engineering@fluidattacks.com',
    packages=['utilities']
)
