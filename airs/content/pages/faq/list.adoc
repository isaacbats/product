:slug: faq/list/
:category: faq
:description: Here we present a compilation of questions and answers to help our clients understand Fluid Attacks' services and how it can benefit your organization.
:keywords: Fluid Attacks, Services, Continuous Hacking, Ethical Hacking, FAQ, Questions, Answers, Pentesting
:faq: yes

= FAQ

=== 1. What is Continuous Hacking?
Continuous Hacking is a security testing service
that allows the hacking process to begin at an early stage
in the software development cycle.
Its purpose is to guarantee `100%` testing coverage of the application.


=== 2. What are the benefits of Continuous Hacking?
Continuous Hacking:
. Minimizes the cost of remediation (repair) of a vulnerable security risk
while the software is in development rather than when it is in production.

. Reduces application certification time to zero
because the hacking is done during development.

. Provides clear and detailed information about vulnerable security risks
and facilitates a coordinated effort between external project personnel
(`Fluid Attacks` experts) identifying security risks,
and internal project personnel (client company)
fixing security issues without delays.

=== 3. In what industries does Fluid Attacks have experience?
Along our career trajectory we have been working with companies
from different sectors, such as financial, transportation,
industrial, consumer, communications, technology and utilities.

=== 4. How does Fluid Attacks stay on top of new techniques and changes?
We ensure to constantly research and keep updated
with the new techniques and changes related to cybersecurity and `IT`.
Through link:../../blog[blog entries]
we keep our team updated with the state of art.

=== 5. What are the necessary inputs and requirements for Continuous Hacking?
The necessary inputs and requirements are:

. *Phase 1:* Access to the integration branch of the repository
for the not-yet-deployed application’s source code.
Ethical Hacking focuses on the source code.

. *Phase 2:* When the project has a deployed application
(Integration Environment), the hacking coverage expands
to include application security testing.

. *Phase 3:* This phase applies only if the infrastructure
supporting the application is defined as code and kept
in the integration branch of the repository referred to in Phase 1.
This phase includes infrastructure hacking.

=== 6. What are the technical conditions that I must meet for Continuous Hacking?

Access to `Git` and a monitored environment in the branch are required,
through automated Linux.
The following environments are not supported:

. Access through a `VPN` that only runs on `Windows`.
. `VPN` in `Windows` that requires manual interaction such as an `OTP` token.
. `VPN` Site to Site.

=== 7. What type of hacking is included in Continuous Hacking?
Continuous Hacking includes source code analysis,
application hacking (see question 5),
and infrastructure hacking (see question 5).

=== 8. What is a vulnerability?
A vulnerability is anything that represents a security risk
(Integrity, Availability, Confidentiality, Non-repudiation)
to the application.

=== 9. What is an active author and how can I identify it?
An active author is a user with access to the `Git` repository
who makes changes to the stored code in the repository during
the analyzed month.

=== 10. Does Continuous Hacking use a series of automated tools or is it the result of a manual (by hand) process?
Automated tools, by themselves,
are not capable of extracting sensitive business information,
such as client or employee information.
In our Continuous Hacking service, we use a series of tools
which are acquired and developed by us at `Fluid Attacks`,
as well as a detailed review process performed by our expert technical staff.
We go the extra mile because automated tools present the following problems:

. Vulnerability leakages (detection of a minimal percentage
of existing security risk vulnerabilities).

. Detected vulnerabilities are primarily false positives.

. Incapability of combining individual vulnerabilities
in order to reveal additional vulnerabilities
which may be an even greater security risk
than the individual vulnerabilities alone.

=== 11. If Continuous Hacking includes a manual review, how does Fluid Attacks ensure that development cycles are not slowed down?
Continuous Hacking is first performed on the source code.
This allows for hacking and development to occur simultaneously,
which in turn minimizes the dependency on functional environments,
as well as the need for coordination between hackers and developers.
The decisions regarding which findings are prioritized for each sprint
rest solely with the client.
Unless we are dealing with a company with daily `CI/CD`
(Continuous Integration/Continuous Deployment),
not all sprints generate code eligible for release and deployment,
which improves the remediation (repair) time for detected vulnerabilities.

=== 12. If Continuous Hacking is done manually, how can a big project move rapidly and expand as more active authors (developers) join the team?
Standard Continuous Hacking
covers `95%` of all business applications being developed,
as the subscription is based on the number
of active developers in the project and this defines the amount of resources
assigned to the project.

=== 13. If Continuous Hacking is done manually, how does it move rapidly when a client has a big application portfolio that is constantly increasing?
Based on our historical data,
and thanks to our recruitment and training capabilities,
as well as our ability to innovate internal processes,
we are fully capable of taking on
between `5` and `10` new applications each month.

=== 14. What kind of information does Fluid Attacks need in order to provide a quotation?

To provide a proposal, we need to determine
what the target of evaluation (scope) will be.
So, we require the following information:

. *One-shot hacking* (by project):

.. How many ports are included in the scope?
.. How many inputs of applications are included in the scope?
.. How many `LoC` are included in the scope?
We recommend running link:https://github.com/AlDanial/cloc[`CLOC`]
in order to facilitate quantification.

*Note:* It would be desirable to obtain the access credentials
(standard user, not privileges) to the applications
in cases where this will be included.

. *Continuous hacking* (`SDLC`):
Under this model, we need to know how many active authors
will be involved in the project.

Regarding the health check estimation,
the same considerations apply as for one-shot hacking,
so the client should provide the above-mentioned information as well.

=== 15. Does the cost of Continuous Hacking vary according to the scope or development phases?
Yes. The service cost varies depending on the number of active authors
identified in the project each month.

=== 16. Why is it necessary for Continuous Hacking to have access to the source code stored in the repository?
Continuous Hacking needs access to the source code
because it is based on continuous attacks
on the latest version available.

=== 17. When does Continuous Hacking begin?
Continuous Hacking begins immediately after receiving the purchase order.

=== 18. Why is there a month 0 and how does setup work?

Month `0` begins the test setup and is the start of the monthly payment.
A project leader is assigned who is responsible
for managing the connection of environments, profiling, user creation,
allocation of privileges, and all the necessary inputs
to begin the review without setbacks.

=== 19. Is it possible to hire On-the-Premises Continuous Hacking?
No. Due to the operational model that supports Continuous Hacking,
it can only be done remotely.

=== 20. Is it possible to schedule follow-up meetings?
Yes. All applications covered by the contract for Continuous Hacking
are assigned to a specific project leader who is available
to attend all necessary meetings.
We simply require sufficient notice of an impending meeting
in order to schedule availability.

=== 21. How is a project’s progress determined?
A project’s progress and current state is determined
using the following metrics:
. Source code coverage indicator.
. Percentage of remediated (repaired) security risk vulnerabilities.

=== 22. When does Continuous Hacking end?
Continuous Hacking is contracted for a minimum of `12` months
and is renewed automatically at the end of the `12-month` time period.
Continuous Hacking ends when we receive a written request
through previously defined channels to terminate the contract.

=== 23. Can the contract be canceled at any point in time?
You can cancel your contract at any time after the fourth month.
Cancellation can be requested through any communication channel
previously defined in the contract.

=== 24. When the coverage of my application reaches 100%, is Continuous Hacking suspended until new code is added to the repository?
No. Even if `100%` of coverage is reached,
we continue checking already attacked source code to identify
any possible false negatives,
including components developed by third parties in our hacking process.

=== 25. How is the severity and criticality of the vulnerability calculated?
`Fluid Attacks` uses link:https://www.first.org/cvss/[CVSS]
(Common Vulnerability Scoring System),
a “standardized framework used to rate
the severity of security vulnerabilities in software.”
It gives us a quantitative measure ranging from `0` to `10`,
`0` being the lowest level of risk and `10` the highest
and most critical level of risk,
based on the qualitative characteristics of a vulnerability.

=== 26. How do I get information about the vulnerabilities found in my application?
Continuous Hacking has an interactive reporting platform
called link:../../products/integrates/[Integrates].
Integrates gives all project stakeholders access
to details concerning vulnerabilities reported by `Fluid Attacks`.
We have recently released link:https://gitlab.com/fluidattacks/integrates[`Integrates`]
source code to our link:https://gitlab.com/fluidattacks[public repository].

=== 27. What types of reports does Continuous Hacking generate?
Continuous Hacking generates and delivers,
through link:../../products/integrates/[Integrates],
a technical report available in `Excel` and/or `PDF` format
during the execution of the project contract.
Once the project ends, Integrates delivers a presentation
and an executive report, also in `PDF` format.

=== 28. What happens after Fluid Attacks reports a vulnerability?
Once `Fluid Attacks` reports a vulnerability,
the main objective for developers is to eliminate it.
Through Integrates, a client company’s developers can access
first-hand detailed information regarding a vulnerability
in order to plan and execute corrective measures
to remove it from the application.

=== 29. What communication does Fluid Attacks provide? When? How?
For Continuous Hacking, communication takes place
between developers and hackers on a day-to-day basis via Integrates.
In One-shot Hacking, communication is handled
through the project manager (`PM`) as a single point of contact (`SPOC`).

=== 30. How does Fluid Attacks know a vulnerability has been eliminated or remediated?
Through link:../../products/integrates/[Integrates],
any user with access to the project can request verification
of a remediated vulnerability.
A request for verification that a remediated vulnerability
no longer poses a risk must be accompanied by notification from you
that the planned remediation has been executed.
We then perform a closing verification
to confirm the effectiveness of the remediation.
Results of the closing verification are then forwarded
to the project team by email.

=== 31. How many closing verifications are included in Continuous Hacking?
Continuous Hacking offers unlimited closing verifications.

=== 32. Why do I need to notify Fluid Attacks that a remediation has been executed if you already have access to the source code repositories?
One of Continuous Hacking’s objectives
is to maintain clear and effortless communication
between all project members.
This is accomplished when you notify us
because the message goes through Integrates and by doing so,
the entire project team is notified.

=== 33. What happens if I do not consider something a vulnerability?
Within link:../../products/integrates/[Integrates] there is a comment section.
A client company can post its reasons
for believing a vulnerability finding is not valid.
Our experts and all other project members
can then interface and discuss
the relative merits of the vulnerability finding
as well as the validity of it as a security risk,
and a final determination can be made.

=== 34. Do all reported vulnerabilities have to be remediated?
No. However, this decision is made entirely by the client,
not by us, and the client assumes all responsibility
for possible negative impacts of non-remediation.
In link:../../products/integrates/[Integrates], under the treatment option,
a client company indicates whether it will remediate
or assume responsibility for an identified vulnerability.

=== 35. If a client decides not to remediate a vulnerability, thus assuming responsibility for it, is it excluded from the reports and Integrates?
No. Reports and Integrates include information regarding all vulnerabilities,
along with whether vulnerabilities were remediated or not.
Your report and Integrates will include
all the information with nothing excluded.

=== 36. If the application is stored along multiple repositories, can they all be attacked?
Yes, with one condition.
The code must be stored in the same branch in each repository.
For example: If it is agreed that all attacks
will be performed on the `QA` branch,
then this same branch must be present in all of the repositories
included for Continuous Hacking.

=== 37. If I have code that was developed a long time ago, is it possible to still use Continuous Hacking?
Yes, it is still possible to use Continuous Hacking.
There are two possible options available:

. A Health Check can be performed testing all existing code.
Then, Continuous Hacking is executed as usual
within the defined scope (see question 11).
This option is better suited for applications under development.

. Start with the standard limits (see question 10),
increasing the coverage on a monthly basis until `100%` is reached.
This option is better suited for applications no longer in development.

=== 38. What does Fluid Attacks do to catch up with the revision of the existing code before starting the hacking process?
We recommend that application development
and the hacking process begin simultaneously.
However, this is not always possible.
To catch up with developers,
we perform a link:../../services/continuous-hacking/healthcheck/[`Health Check`] (additional fees apply).
This means all versions of the existing code
are attacked up to the contracted starting point
in addition to the monthly test limit.
This allows us to catch up with the development team
within the first `3` contract months.
Then, we continue hacking simultaneously with the development team
as development continues.

=== 39. What happens if I don't want to perform a Health Check, but I want the Continuous Hacking service?
This is a risky choice.
Not performing a Health Check means there will be code
that is never going to be tested and, therefore,
it's not possible to know what vulnerabilities may exist in it;
those vulnerabilities are not going to be identified.
We guarantee that `100%` of the code change
is going to be tested, but what cannot be reached, cannot be tested.

=== 40. Do the repositories need to be in a specific version control system?
Continuous Hacking is based on using `Git` for version control.
Therefore, `Git` is necessary for Continuous Hacking.

=== 41. Does Fluid Attacks keep or store information regarding the vulnerabilities found?
Information is only kept for the duration of the Continuous Hacking contract.
Once the contract has ended, information is kept for `7` business days
and then deleted from all our information systems.

=== 42. How will our data be erased?
`Integrates` uses an automated erasing process,
removing all the project information from our systems
and generating a `Proof of Delivery` signed via link:https://www.docusign.com/[`Docusign`].

=== 43. Does Continuous Hacking require any development methodology?
No. Continuous Hacking is independent
of the client’s development methodology.
Continuous Hacking test results become a planning tool
in future development cycles.
They do not prevent the continuation of development.

=== 44. Will Fluid Attacks periodically do presentations via teleconferencing? How do I set one up?
Yes. `Fluid Attacks` can schedule periodic presentations via teleconferencing.
To set up a teleconference presentation, you will need to provide us
with the emails of attendees and `3` optional time periods
of `1-hour` duration for the teleconference.
We will then notify you of the best time for the teleconference
based on your availability and ours,
and send emails to your list of attendees
inviting them to participate.

=== 45. Does the use of the Continuous Hacking model depend on the type of repository where the code is stored?
No. The client can use whatever repository they deem appropriate.
We only require access to the integration branch
and its respective environment.

=== 46. Who would be performing the work?
Our designated team of hackers.

=== 47. Can we see resumes?
Yes, you can access the `LinkedIn` profiles of some members of our team
on our link:../../about-us/people[people] page.

=== 48. What certifications does Fluid Attacks have?
Please refer to our link:../../about-us/certifications/[certifications] page
for further information.

=== 49. Do I lose my property rights if Fluid Attacks reviews my source code?
No. Reviewing your code in no way compromises
your proprietary rights to that code.

=== 50. Does Fluid Attacks have a tool that enables automatic remediation and closing of previously confirmed vulnerabilities?
Yes. link:../../products/asserts/[Asserts] is `Fluid Attacks'` automated engine,
checking remediation of previously confirmed vulnerabilities.
link:../../products/asserts/[Asserts] operates in the `JOB` of continuous integration.
It can break the build sent by the programmer in the event
of a breach of security requirements.
We have recently released link:https://gitlab.com/fluidattacks/asserts[`Asserts`]
source code to our link:https://gitlab.com/fluidattacks[public repository].

=== 51. Does Continuous Hacking only focus on source code? Is it possible to include the infrastructure associated with the app?
We have improved the Continuous Hacking model
to now include infrastructure within the Target of Evaluation (`ToE`).
This includes the application's ports, inputs,
infrastructure, and of course the application itself.

=== 52. What external tools does Fluid Attacks use to perform pentesting?
We use link:https://portswigger.net/burp[Burp Suite] for web testing,
and link:https://www.immunityinc.com/products/canvas/[CANVAS] and
link:https://www.coresecurity.com/products/core-impact[Core Impact]
for infrastructure testing with additional exploits.

=== 53. How will our data be transmitted?
It is up to you, however, we recommend the use of `HTTPS`
for application tests and `SSH` (`git`) for source code analysis.

=== 54. How will our data be stored?
* link:https://aws.amazon.com/[`AWS` on the cloud] (mainly `S3` and
  link:https://aws.amazon.com/dynamodb/[`DynamoDB`],
  all security enabled)
* Hackers' computers with disk encryption in all partitions.

=== 55. Where does Integrates run?
The platform, link:../../products/integrates/[Integrates], runs in the cloud.

=== 56. Does Fluid Attacks manage the access credentials to Integrates?
No. We use federated authentication.
`Google` and `Azure` (`Microsoft 360`)
are the entities which validate your user access credentials.

=== 57. Is it possible to activate the double authentication token?
Yes, it is, and we recommend you do so.
Using double authentication will increase
the security level of your credentials.
This will help prevent unauthorized users
from accessing and compromising your information.
This feature is enabled through `Gmail` or `Azure`.

=== 58. If I make a commit, how long does it take you to review the commit and test it?
The goal is `100%` coverage.
Therefore, there will be results
regarding system vulnerabilities continuously throughout the contract period.
We take into account all pushes to the tested branch,
which are monitored using automated scripts (robots)
that extract and analyze the changes made to the source code every night.

=== 59. Does `Fluid Attacks` test every time I make a push in the subscription branch?
During the execution of a project, the following scenarios can occur:

. Application in development without overdue code (`100%` coverage):
The robot detects the change and generates the updated control files.
This means that no specific file or commit is audited,
but rather the change analysis performed by the robot is incorporated
when the hackers attack the application,
thus allowing them to take into account the changes made.

. Application in production without overdue code (100% coverage):
Even when there are no changes, the application is attacked.
Internally, we have processes that help us identify
why we haven’t found vulnerabilities in the application in 7, 14 and 21 days.
These processes include such things as hacker rotations
or increasing the number of hackers assigned to the project
in order to find undiscovered vulnerabilities.

. Application in development with overdue code (`<100%` coverage):
Same as the first scenario, but attacks are only related
to the change that was made.
The attack surface that existed before the subscription point is not attacked.

. Application in production with overdue code (`<100%` coverage):
Same as the second scenario, but if in a specified month there is no new code,
it is hacked only to the extent of the changes
made by `one` active author in `one` previous month.

=== 60. What options for retesting are available?
link:../../services/one-shot-hacking[One-shot Hacking] includes one retest.
link:../../services/continuous-hacking/[Continuous Hacking] includes infinite retests
during the subscription time.

=== 61. What are the scheduled activities during the Continuous Hacking test?
Once the setup has been completed,
and everything is ready for the service to begin, the security tests start.
The steps are as follows:

. Approval request (purchase order confirmed).
. Project leader assignment.
. The project leader schedules the start meeting (teleconference).
. Service condition validation.
. Supplies request (access to environments and code).
. The project leader receives supplies,
and programs the setup of the verification and access robots.
. The project leader creates an admin user in link:../../products/integrates/[Integrates] for the client.
. The admin user invites all project stakeholders including the developers.
(They must have `Google Apps` or `Office365`.)
. Vulnerabilities are reported in link:../../products/integrates/[Integrates].
. Project stakeholders access vulnerabilities and start remediation.
. If any questions or problems arise,
they can be addressed through the comments or chat available in link:../../products/integrates/[Integrates].
. When the client has remediated the reported vulnerabilities,
they may request validation of their repairs through link:../../products/integrates/[Integrates].
. Our hacker performs the closure verification and updates the report.
. Steps `3` - `7` are repeated until the subscription ends.

=== 62. What technical conditions do I need to meet if I want to use Asserts inside my continuous integrator?
Asserts runs on any continuous integration platform
that supports `Docker` (`Docker engine 18.03.1`)
and has access to the internet.

=== 63. Is there documentation for Asserts?
Yes, it is available on the link:https://fluidattacks.com/products/asserts/[Asserts page].

=== 64. Is it possible to group multiple applications into one subscription? How would I recognize the vulnerabilities within each application?
According to the active authors model,
it is possible to create a large cell with all the developers
or to divide it into applications according to the client's needs.
When managing only one cell, it is important to consider the following:
* All users in the project can see all the vulnerabilities
of the application inside the same cell.
* When the same vulnerability appears in several applications,
the only way to identify/locate each one in each individual application
is by checking the vulnerability report under the heading "location".
There, it will specify where each vulnerability can be found.

=== 65. Is it possible to change the environment when the subscription is already active?
Yes, it is possible under the condition that the new environment
be the same branch environment where the source code is reviewed,
thus allowing us to test the same version of the change
both statically and dynamically.

=== 66. How will you ensure the availability of my systems and services while the test is taking place?
It is possible to cause an accidental `DoS` during the hacking service.
We recommend including only the staging phase in the scope.
However, many clients decide to also include
the production stage in the tests.
It is unusual for us to take down environments
because when we foresee a possible breakpoint,
we ask the client for a special environment
within which to carry out the test.

=== 67. What happens if I want to review different environments of the same application?
The service includes the environment of the reviewed code (see question 52).
It is possible to include different environments for an additional fee.

=== 68. If I ask a question in the comment system, how long does it take to get an answer?
All questions made through the vulnerabilities comment system,
have a `4` business hours `SLA`. M - F
from `8AM` to `12` noon and `2PM` to `6PM`
(`UTC-5` Colombia = same as Eastern Standard Time `USA`).
`SLA` is not contractually defined, it is our value promise.

=== 69. Do you have liability insurance?
Yes, `1M USD` coverage.
