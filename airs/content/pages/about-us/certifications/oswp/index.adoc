:slug: about-us/certifications/oswp/
:description: Our team of ethical hackers and pentesters counts with high certifications related to cybersecurity information.
:keywords: Fluid Attacks, Ethical Hackers, Team, Certifications, Cybersecurity, Pentesters, Whitehat Hackers
:certificationlogo: logo-oswp
:alt: Logo OSWP
:certification: yes

= Offensive Security Wireless Professional (OSWP)

`OSWP` is the only professional certification
in practical wireless attacks in the security field today.
In a hands-on exam, an `OSWP` must prove they have the skills
to do 802.11 wireless audits using open source tools.
