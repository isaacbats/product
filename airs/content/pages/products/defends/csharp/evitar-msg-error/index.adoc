:slug: products/defends/csharp/evitar-msg-error/
:category: csharp
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la programacion segura en C Sharp al evitar mensajes de error en autenticación. Los mensajes de error pueden contener información que puede ser aprovechada por un atacante para vulnerar la aplicación.
:keywords: C Sharp, Autenticación, Mensaje, Error, Seguridad, Buenas Prácticas
:defends: yes

= Evitar Mensajes de Errores en Autenticación

== Necesidad

No emitir mensajes de error identificables en la autenticación.

== Contexto

A continuación se describen las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se está desarrollando una aplicación en +C Sharp+.
. Los mensajes de error de autenticación muestran
qué parte de la autenticación falló.

== Solución

Al momento de programar una aplicación
expuesta a Internet es necesario considerar
al usuario final como uno malintencionado.
Un usuario malintencionado puede aprovecharse de vulnerabilidades conocidas,
descuidos, fallos o eventos inesperados por la aplicación
para efectuar un ataque.
Por este motivo, es altamente recomendable desarrollar
utilizando buenas prácticas de programación.
Las buenas prácticas de programación consisten en una serie de criterios
que sirven para mejorar la seguridad de las aplicaciones
desarrollando código robusto y computacionalmente seguro.

Una buena práctica de programación
consiste en manejar de forma adecuada
los mensajes de error que se muestran al usuario.
Si al momento de validar las credenciales de un usuario,
la aplicación detecta que el nombre de usuario
y/o la contraseña no son válidos,
la aplicación no debe mostrar qué parte de la autenticación falló.
Esto se debe a que el mensaje de error
puede ser utilizado por un atacante para crear una lista
de nombres de usuario y contraseñas válidos,
resultando así en un robo de credenciales.

En este artículo mostraremos cómo configurar adecuadamente
los mensajes de error de autenticación en +C Sharp+.
Para ello debemos seguir la siguiente serie de pasos:

. Se define la condición que valida si los datos de autenticación
del usuario son correctos o no.
Inmediatamente después de hacer la consulta en la base de datos.
Posteriormente se define el nuevo mensaje de error de autenticación.
Se recomienda colocar un mensaje de error genérico
que no dé mayor información <<r2 , ^[2]^>>.
Como se muestra en el siguiente código:
+
.login-errormsg.cs
[source, csharp, linenums]
----
{
  using (DataTable dt = LookupUser(textBoxUsername.Text, textBoxPassword.Text )){
    if (dt.Rows.Count == 0){
      textBoxUsername.Focus();
      MessageBox.Show("La combinación de credenciales no es válida.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      textBoxUsername.Focus();
      return;
    }
  }
}
----

. Cabe resaltar que aplicando el fragmento de código anterior,
cuando la autenticación no se realiza exitosamente,
el mensaje de error no muestra
si error se produjo en el nombre de usuario o la contraseña.

== Referencias

. [[r1]] link:https://www.owasp.org/index.php/Testing_for_User_Enumeration_and_Guessable_User_Account_(OWASP-AT-002)[OWASP-AT-002 Testing for User Enumeration and Guessable User Account].

. [[r2]] link:https://www.owasp.org/index.php/Error_Handling[Generic Error Messages - OWASP Error Handling].

. [[r3]] link:https://www.owasp.org/index.php/Improper_Error_Handling[OWASP - Improper Error Handling].

. [[r4]] link:../../../products/rules/list/225/[REQ.225 Respuestas de autenticación adecuadas].
