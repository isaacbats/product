:slug: products/defends/aix/conocer-privilegios-rbac/
:category: aix
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la asignación segura de privilegios root a usuarios no privilegiados. Es importante establecer privilegios para restringir el acceso a partes críticas de la aplicación a usuarios sin permisos.
:keywords: AIX, Seguridad, RBAC, CLI, Privilegios, Root.
:defends: yes

= Conocer los Privilegios RBAC de un Proceso

== Necesidad

Conocer los privilegios +RBAC+ de un proceso y/o del sistema en +AIX+.

== Contexto

A continuación se describen las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se dispone de un sistema operativo +AIX+ versión 6 o superior.
. Se dispone de acceso a la interfaz de comandos (+CLI+) del sistema anterior.

== Solución

. Un sistema +Unix+ tradicional
realiza múltiples verificaciones de seguridad
dentro de las llamadas al sistema, por ejemplo:

* Si el +EUID+ (+UID+ efectivo) del proceso tiene los privilegios
para invocar dicha llamada al sistema.

* Si el +EUID+ (+UID+ efectivo) del proceso es 0 (+root+).

. Por ejemplo, el código fuente de una llamada al sistema
tendría la siguiente verificación
para permitir que dicha llamada al sistema
solo fuera ejecutada por el usuario +root+:
+
.test.sh
[source, bash, linenums]
----
... if (getuid() == 0) { /* acción privilegiada */ } ...
----

. Con este mecanismo de control,
un usuario que requiera ejecutar dicha llamada al sistema
o acción privilegiada, requiere:

* Permisos de ejecución en el comando que ejecuta la llamada al sistema.
* Conocer la contraseña de +root+
para iniciar una +shell+ y ejecutar el comando.
* O el comando en cuestión debe tener habilitado el bit +SUID+.

. Debido a esta situación,
y a la necesidad permanente
de estar otorgando privilegios de +root+ a usuarios no privilegiados
para que puedan ejecutar tareas administrativas<<r1,^[1]^>>,
+AIX+ ha definido un sistema de control de acceso llamado +RBAC+
o +Role Based Access Control+.

. En este tipo de sistema de control de acceso,
las verificaciones de acceso a las llamadas al sistema
ocurren dentro del núcleo así:
+
[source, bash, linenums]
----
... if ("getprivs() == PV_XXX") { /* acción privilegiada */ } ...
----
. Esto significa que las acciones privilegiadas del núcleo
no ocurrirán dependiendo del +EUID+ del proceso,
sino dependiendo de los privilegios que tenga asignado el proceso.

. Esto implica que el sistema operativo +AIX+
define unos privilegios específicos (estándar, fijos, aproximadamente 100)
que se pueden asignar a un proceso
y con los cuales se harán las verificaciones correspondientes
dentro de las llamadas al sistema.

. Los privilegios definidos para un sistema +AIX+
pueden verse mediante la orden:
+
[source, shell, linenums]
----
% lspriv
PV_ROOT
PV_AU_
PV_AU_ADD
PV_AU_ADMIN
PV_AU_READ
...
----

. La orden anterior al ser ejecutada
y acompañada del parámetro +-v+
muestra el significado de cada privilegio:
+
[source, shell, linenums]
----
% lspriv -v
PV_ROOT Allows a process to pass any non-SU privilege check.
PV_AU_ Equivalent to all Auditing privileges (PV_AU_*) combined.
PV_AU_ADD Allows a process to record/add an audit record.
PV_AU_ADMIN Allows a process to configure and query the audit system.
PV_AU_READ Allows a process to read a file marked as an audit file.
...
----

. Como puede observarse a partir de los nombres de los privilegios,
estos forman una jerarquía,
de forma que pueda asignarse privilegios
a un proceso con granularidad fina (detallada) o gruesa (agrupada).

. La jerarquía de privilegios es la siguiente<<r2,^[2]^>>:
+
.Jerarquía de nombres de privilegios.
[cols="^,,^",options="header"]
|====
|Privilegio
|Significado
|Número de Privilegios

|+PV_NET_+.
|Privilegios sobre las llamadas al sistema relacionadas con la red.
|4.

|+PV_FS_+.
|Privilegios sobre las llamadas al sistema
relacionadas con sistemas de archivos.
|9.

|+PV_AU_+.
|Privilegios sobre las llamadas al sistema relacionadas con auditoria.
|5.

|+PV_KER_+.
|Privilegios sobre las llamadas al sistema
relacionadas con el núcleo (+kernel+).
|21.

|+PV_PROC_+.
|Privilegios sobre las llamadas al sistema relacionadas con los procesos.
|13.

|+PV_DAC_+.
|Privilegios sobre las llamadas al sistema relacionadas con los autorización.
|7.

|+PV_SU_+.
|Todos los privilegios propios del super-administrador.
|3.

|+PV_ROOT+.
|Todos los privilegios excepto los de +PV_SU_+.
|-.

|====

. Finalmente puede observarse los privilegios
que un programa requiere para ejecutarse mediante la siguiente orden:
+
[source, shell, linenums]
----
# tracepriv -ef {comando}
----
. Por ejemplo, para conocer los privilegios necesarios
para ejecutar el comando +/usr/bin/chgrp+ se realizará:
+
[source, shell, linenums]
----
# touch /tmp/file # cp /usr/bin/chgrp /usr/bin/chgrpr # tracepriv -ef /usr/bin/chgrpr security /tmp/file
557304: Used privileges for /usr/bin/chgrpr: PV_DAC_O PV_FS_CHOWN
----
. Indicando que para ejecutar el comando +/usr/bin/chgrpr+
los privilegios requeridos para ejecutar el mismo en modo +RBAC+
(verificación a partir de privilegios y no verificación
a partir de credenciales +DAC+) son: +PV_DAC_O+ y +PV_FS_CHOWN+.

== Referencias

. [[r1]] link:https://www.ibm.com/support/knowledgecenter/ssw_aix_61/com.ibm.aix.base/kc_welcome_61.htm[IBM AIX V6.1 documentation].
. [[r2]] link:https://www.ibm.com/support/knowledgecenter/en/ssw_aix_61/com.ibm.aix.security/rbac_aix_privs.htm[AIX privileges].
