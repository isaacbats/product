:slug: products/defends/cobol/manejar-error-texto/
:category: cobol
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la programacion segura en COBOL al manejar adecuadamente los errores en las operaciones de texto. Las operaciones de texto en cobol pueden arrojar excepciones que filtren información sensible.
:keywords: Cobol , Seguridad, Buenas Prácticas, Error, Operación, Texto.
:defends: yes

= Manejar Errores en Operaciones de Texto

== Necesidad

Manejar errores en operaciones sobre cadenas de texto en +COBOL+.

== Contexto

A continuación se describe las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se está desarrollando una aplicación en +COBOL+.
. La aplicación realiza operaciones sobre cadenas
con las instrucciones +STRING+ o +UNSTRING+.
. El código sólo debe realizar las funciones
para las cuales fue diseñado
y no acciones colaterales<<r1,^[1]^>>.
. El sistema debe evitar difundir información sensible
al momento de ocurrir un evento excepcional<<r2,^[2]^>>.

== Solución

. Una característica del lenguaje
es que limita el tamaño de los valores,
al tamaño definido en la división +DATA DIVISION+.
Esto podría generar agujeros de seguridad
al no tener en cuenta valores por fuera del límite de longitud.

. En el manejo de cadenas de texto
es común encontrar operaciones de concatenación,
en +COBOL+ se logra a través del uso de +STRING+
y se evitan los desbordamientos de longitud
a través del uso de la cláusula +ON OVERFLOW+.
Además, esta situación puede ser también encontrada en el uso de +UNSTRING+.

. Para la instrucción +STRING+,
el código contenido en +ON OVERFLOW+
es llamado cuando el valor del puntero es:

* Menor a 1.

* Mayor a la longitud del campo de recepción.

. Para la instrucción +UNSTRING+,
el código contenido en +ON OVERFLOW+ es llamado cuando:

* El valor del puntero es menor a 1.

* El valor del puntero
es mayor que la longitud del campo de envío.

* Se ha actuado sobre todos los campos de acogida
y existen caracteres sin examinar en el campo de envío.

. Lo anterior se ilustra mediante un ejemplo.
En primer lugar se define la división +IDENTIFICATION DIVISION+^<<r3,[3]>>,<<r4,[4]>>^:
+
.cobolof.cbl
[source, cobol,linenums]
----
       IDENTIFICATION DIVISION.
      ******************
      * Identification *
      ******************
       PROGRAM-ID. COBOLOF.
----
. En la división +DATA DIVISION+<<r5,^[5]^>> se declaran tres variables:

* +W01-FIRSTNAME:+ El primer nombre de longitud máxima 10.

* +W02-LASTNAME:+ El apellido de longitud máxima 10.

* +W03-FULLNAME:+ El nombre completo compuesto por primer nombre
y apellido de longitud máxima 10.
Desde este punto se puede notar que hay un error de diseño,
la longitud máxima para el nombre completo
debería ser la suma de las dos anteriores variables (Longitud máxima de 20).

+
[source, cobol,linenums]
----
      ********
      * Data *
      ********
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 W01-FIRSTNAME PIC X(10) VALUE "fluidattacks".
       01 W02-LASTNAME  PIC X(10) VALUE "Group".
       01 W03-FULLNAME  PIC X(10).
----
. Se concatena las variables de nombre (+W01-FIRSTNAME+)
y apellido (+W02-LASTNAME+):
+
[source, cobol,linenums]
----
      ********
      * Main *
      ********
       PROCEDURE DIVISION.
       MAIN.
           STRING W01-FIRSTNAME SPACE W02-LASTNAME
           DELIMITED BY SPACE
           INTO W03-FULLNAME
----
. En el caso de presentarse un desbordamiento en la cadena,
el flujo de ejecución se transfiere a +STRING-OVERFLOW+:
+
[source, cobol,linenums]
----
       ON OVERFLOW
           PERFORM STRING-OVERFLOW.
----
. Si no ocurre un error,
se despliega en pantalla el nombre completo
contenido en la variable +W03-FULLNAME+:
+
[source, cobol,linenums]
----
       DISPLAY "Nombre completo: ", W03-FULLNAME.
       STOP RUN.
----
. En esta sección es posible tratar el desbordamiento como un error,
para el presente ejemplo
se despliega en pantalla una advertencia:
+
[source, cobol,linenums]
----
       STRING-OVERFLOW.
           DISPLAY "String overflow".
           STOP RUN.
----

== Descargas

Puedes descargar el código fuente
pulsando en el siguiente enlace:

. [button]#link:src/cobolof.cbl[cobolof.cbl]# contiene
el código explicado anteriormente sin desbordamiento.

== Referencias

. [[r1]] link:../../../products/rules/list/154/[REQ.154 Eliminar puertas traseras].
. [[r2]] link:../../../products/rules/list/083/[REQ.083 No registrar datos sensibles en bitácoras].
. [[r3]] link:https://www.ibm.com/support/knowledgecenter/en/ssw_ibm_i_73/rzasb/iddiv.htm[IBM - Identification Division].
. [[r4]] link:http://www.escobol.com/modules.php?name=Sections&op=viewarticle&artid=11[Identification Division].
. [[r5]] link:https://www.ibm.com/support/knowledgecenter/en/ssw_ibm_i_73/rzasb/datdivs.htm[Data Division Structure].
