:slug: products/defends/cobol/compilar-programa-estricta/
:category: cobol
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la programacion segura en COBOL al compilar de forma estricta el programa. Los mensajes de compilacion dejan entrever rutas e información que puede ser utilizada para vulnerar una aplicación web.
:keywords: Cobol, Programación, Seguridad, Compilación Estricta, Depurar, Buenas prácticas.
:defends: yes

= Compilar Programa de Forma Estricta

== Necesidad

Compilar programa en +COBOL+ de forma estricta.

== Contexto

A continuación se describe las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se está desarrollando una aplicación en +COBOL+.
. El código debe compilarse
o interpretarse de forma estricta<<r1,^[1]^>>.

== Solución

. A través del parámetro +GENLVL+ del comando +CRTBNDCBL+,
se especifica el nivel de severidad
que determina si un objeto es creado.
La severidad corresponde al nivel de rigurosidad
de los mensajes producidos durante la compilación.
Este parámetro aplica de manera individual
a cada unidad dentro del miembro del archivo fuente.

. Se debe especificar un número de uno
o dos dígitos, desde 0 hasta 29<<r2,^[2]^>>,
el cual es el nivel de severidad que usted desea
para determinar si el objeto es creado o no.
Ningún programa es creado
si algún error ocurre con una severidad igual
o mayor a la especificada.

. La siguiente tabla muestra
los niveles de severidad
para diferentes tipos de mensajes:
+
.Niveles de severidad para diferentes tipos de mensajes.
[options="header"]
|====
|Severidad |Tipo de mensaje
|00 - 04   |Información
|05 - 19   |Advertencia
|20 - 29   |Error
|30 - 39   |Severo
|40 - 99   |Terminal
|====

. Para compilar un programa en +COBOL+ de forma estricta,
se debe usar "0" como argumento en el parámetro +GENLVL+.
Para crear el programa +COBOLSTR+ de la biblioteca +fluidattacks+
con un nivel de severidad 0 (Compilación estricta),
se usa el siguiente comando:
+
[source,cobol,linenums]
----
      CRTBNDCBL
      PGM(fluidattacks/COBOLSTR)
      SRCFILE(fluidattacks/SOURCE)
      SRCMBR(COBOLSTR)
      GENLVL(0)
      REPLACE(*YES)
----

. El anterior programa es creado exitosamente,
el código fuente utilizado para esta solución
y que contiene la estructura básica de un programa en +COBOL+,
es el que puede observar a continuación:
+
.cobolstr.cbl
[source,cobol,linenums]
----
       IDENTIFICATION DIVISION.
       PROGRAM-ID. COBOLSTR.
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       PROCEDURE DIVISION.
       MAIN.
           DISPLAY "Programa estricto".
           STOP RUN.
----
+
En el anterior bloque de código se puede observar
las cuatro divisiones de las que
se compone un programa en +COBOL+:

* +IDENTIFICATION DIVISION:+ mediante la sentencia +PROGRAM-ID+
se especifica el nombre del programa.
En este caso es +COBOLSTR+.

* +ENVIRONMENT DIVISION:+ el objetivo de esta división
es describir el ambiente
sobre el cual se desarrolló
y va a ser ejecutado el programa<<r3,^[3]^>>.
En este caso dicha división no es implementada.

* +DATA DIVISION:+ en esta división se declaran
nombres de campos, registros, variables, etc.
Es decir, se especifica cada dato utilizado por el programa<<r4,^[4]^>>.
En este caso dicha división no es implementada.

* +PROCEDURE DIVISION:+ en esta división se definen
todos los procesos necesarios
para que el programa funcione.
Dicho de otra manera,
consta de todo el algoritmo del programa<<r5,^[5]^>>.
Para este caso, el programa además de
ser compilado de forma estricta,
imprime un texto en pantalla
mediante la instrucción +DISPLAY+.

== Descargas

Puedes descargar el código fuente
pulsando en el siguiente enlace:

. [button]#link:src/cobolstr.cbl[cobolstr.cbl]# contiene
todas las instrucciones +COBOL+ del programa.

== Referencias

. [[r1]] link:../../../products/rules/list/157/[REQ.157 Compilación estricta].
. [[r2]] link:https://www.ibm.com/support/knowledgecenter/en/ssw_ibm_i_72/cl/crtcblpgm.htm[Create COBOL Program].
. [[r3]] link:http://www.escobol.com/modules.php?name=Sections&op=printpage&artid=12[Environment Division].
. [[r4]] link:http://www.escobol.com/modules.php?name=Sections&op=printpage&artid=13[Data Division].
. [[r5]] link:http://www.escobol.com/modules.php?name=Sections&op=printpage&artid=14[Procedure Division].
