:slug: products/defends/aspnet/cifrar-contrasenas-una-via/
:category: aspnet
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la configuracion segura de contraseñas en ASP.NET al utilizar distintas funciones de resumen criptográfico para proteger información crítica y confidencial de la aplicación.
:keywords: ASP.NET, Seguridad, Contraseña, Hash, MD5, SHA1.
:defends: yes

= Cifrar contraseñas en una vía

== Necesidad

Se requiere usar algoritmos criptográficos
para cifrar información confidencial.

== Contexto

A continuación se describen las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. La aplicación esta hecha en cualquier lenguaje
del +.NET Framework+ (+C#+, +VB+).
. La aplicación requiere cifrar información confidencial.

== Solución

Cuando se habla de seguridad informática,
es común tratar temas
como la configuración segura de contraseñas.
Para ello es posible abordar esta temática
desde distintos puntos de vista,
utilizando diversas soluciones.
Para el caso de este artículo se utilizarán
diversas técnicas de tipo +hash+
para cifrar contraseñas de forma segura,
disminuyendo de esta forma,
el riesgo de que un posible atacante
sea capaz de obtenerlas y causar daños
u obtener información confidencial.

Una función +hash+ o funcion +digest+,
es un tipo de función muy utilizada
en aplicaciones criptográficas.
Consiste en un algoritmo que recibe como entrada
un mensaje o cadena de caracteres
en texto plano de longitud variable
y lo transforma en una serie de caracteres
con una longitud constante.
Ésto resulta bastante útil,
debido a que es capaz de transformar
textos muy largos en cadenas cortas
que son relativamente seguras y fáciles de transmitir <<r1, ^[1]^>>.
Las funciones +hash+ tienen por objetivo
reforzar la seguridad de la información crítica de una aplicación,
tales como nombres de usuario o contraseñas,
para permitirles resistir ataques malintencionados.

Las funciones criptográficas +hash+
cuentan con propiedades que permiten
cumplir con su objetivo.
Entre ellas se cuenta la detección de modificaciones,
para asegurar que el mensaje
no ha sido alterado por terceros
y el código de autenticación,
que corrobora que el remitente del mensaje
es efectivamente quien dice ser.

Para el caso concreto de +ASP.NET+,
se utiliza el +.NET Framework+
el cual proporciona las siguientes clases
que implementan algoritmos de resumen criptográfico <<r2, ^[2]^>>:
+MD5CryptoServiceProvider+, +SHA1Managed+,
+SHA256MANAGED+, +SHA384MANAGED+ y +SHA512MANAGED+.
Para usar alguna de estas clases se debe usar
el espacio de nombres +System.Security.Cryptography+
al incluir al inicio del programa
la librería con el mismo nombre.
A continuación se muestra el uso de la clase +SHA256MANAGED+.

. El primer paso es obtener un +array+ de bytes
de los caracteres dentro de la cadena.
+
.Hashpass.java
[source,java,linenums]
----
using System.Security.Cryptography;
byte[] datos=Encoding.ASCII.GetBytes(Cadena);
----

. Se establece el tipo de algoritmo a utilizar
para obtener el valor +hash+.
Para especificar otro proveedor,
se debe cambiar +SHA256Managed+
por el respectivo algoritmo a usar.
Entre otros tipos de algortimo +hash+
disponibles para realizar esta conversión
se encuentran: +MD5CryptoServiceProvider+,
+SHA1Managed+, +SHA384MANAGED+, o  +SHA512MANAGED+.
+
[source,java,linenums]
----
HashAlgorithm hashAlgo;
hashAlgo = new SHA256Managed();
----

. Utilizando el método +ComputeHash+
es posible calcular el valor +hash+
y devolver un +array+ de +bytes+.
+
[source,java,linenums]
----
byte[] hash = hashAlgo.ComputeHash(datos);
----

== Referencias

. [[r1]] link:https://es.wikipedia.org/wiki/Funci%C3%B3n_hash[Función Hash]
. [[r2]] link:https://msdn.microsoft.com/en-us/library/92f9ye3s(vs.71).aspx[Cryptography Overview]
