:slug: products/defends/aspnet/codificar-salida/
:category: aspnet
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la programacion segura en ASP.NET para codificar adecuadamente las salidas en aplicaciones web, evitando así la explotación de posibles vulnerabilidades de tipo Cross Site Scripting.
:keywords: ASP.NET, Seguridad, Codificar, Salida, XSS, Cross Site Scripting.
:defends: yes
:table-caption: Tabla

= Codificar salida

== Necesidad

Codificar la salida según el lenguaje correspondiente en +ASP.Net+

== Contexto

A continuación se describen las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se está desarrollando una aplicación en +ASP.Net+.
. La salida de información del sistema
debe estar codificada en el lenguaje correspondiente.<<r1,^[1]^>>

== Solución

. Los ataques por +Cross Site Scripting+
explotan las vulnerabilidades de las aplicaciones Web
que fallan en validar y/o codificar
las entradas embebidas en los datos de respuesta.
Los usuarios maliciosos pueden inyectar +scripts+
en el lado del cliente a los datos de respuesta
causando que el navegador de la víctima
ejecute códigos posiblemente maliciosos.

. La biblioteca gratuita Anti Cross Site Scripting de Microsoft<<r2,^[2]^>>
puede usarse para proteger a los usuarios de este tipo de ataques <<r3,^[3]^>>.
Antes de abordar el tema
de cómo proteger una aplicación de los ataques de +XSS+
es importante resaltar los vectores de ataque
que un usuario malicioso puede usar para lanzar dichos ataques.

. Idealmente, se deben ejecutar estas tareas en la etapa de diseño,
sin embargo, aún es posible realizar el trabajo en las aplicaciones
que ya se encuentren implementadas de esta forma.
Dichas tareas se listan a continuación:

* Revisar todo el código que produce salidas.
* Determinar si la salida incluye parámetros no confiables.
* Determinar el contexto en el que se usa la entrada no confiable.
* Codificar la salida de manera apropiada.

. El mejor criterio para determinar si la entrada es confiable
es asumir que no lo es.
Ejemplos de entradas no confiables incluyen:

* Campos de formulario.
* Cadenas +"Query"+.
* Contenidos de +cookies+.
* Encabezados +HTTP+.

. Cuando se identifique que una entrada no es confiable,
es importante determinar el método de codificación más apropiado.
La siguiente tabla muestra los métodos de codificación asociados <<r4,^[4]^>>:
+
.Uso apropiado de métodos de codificación.
|===
|*Método de Codificación* | *Cuándo se debe usar*

|+HtmlEncode+
|Entrada no confiable usada en salida +HTML+
excepto cuando se asigna a un atributo +HTML+.

|+HtmlAttributeEncode+
|Entrada no confiable se usa en atributos +HTML+

|+XmlEncode+
|Entrada no confiable usada en salida +XML+
excepto cuando se asigne a un atributo +XML+.

|+XmlAttributeEncode+
|Salida no confiable usada en los atributos +XML+.

|+UrlEncode+
|Entrada no confiable usada como un argumento +URL+.

|+UrlPathEncode+
|Entrada no confiable usada como parte de un path +URL+.

|+JavaScriptEncode+
|Entrada no confiable usada dentro de un contexto +JavaScript+.

|===

. Para usarlo se requiere:

* Click derecho en el explorador de soluciones en +Visual Studio+.
* Seleccionar +Add Reference+.
* Seleccionar la pestaña +browse+ y luego adicione +AntiXSSLibrary.dll+.
* Adicionar una directiva utilizando +Microsoft.Security.Application+.
* Cambiar la forma en que se asigna la salida, por ejemplo:
+
.OutputCoded.java
[source, java, linenums]
----
string out_Name = Request.QueryString["Name"];
----
+
reemplazarlo por:
+
[source, java, linenums]
----
string out_Name = Encoder.HtmlEncode(Request.QueryString["Name"]);
----

== Referencias

. [[r1]] link:../../../products/rules/list/160/[REQ.160 Salidas codificadas].
. [[r2]] link:https://www.microsoft.com/en-us/download/details.aspx?id=28589[Microsoft Anti-Cross Site Scripting Library V4.2].
. [[r3]] link:https://www.microsoft.com/en-us/download/search.aspx?q=antixss[Microsoft AntiXSS].
. [[r4]] link:https://msdn.microsoft.com/en-us/library/aa973813.aspx[Microsoft, Protecting the Contoso Bookmark Page].
