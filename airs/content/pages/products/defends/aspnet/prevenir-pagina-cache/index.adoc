:slug: products/defends/aspnet/prevenir-pagina-cache/
:category: aspnet
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la programacion segura en ASPNET al prevenir almacenamiento de páginas en caché. El caché es muy utilizado en aplicaciones web, sin embargo es tambíen una fuente potencial de vulnerabilidades.
:keywords: ASPNET, Seguridad, Prevenir, Caché, Página, Buenas Prácticas.
:defends: yes

= Prevenir almacenamiento de páginas en caché

== Necesidad

Prevenir almacenamiento de páginas en caché en +ASP.NET+.

== Contexto

A continuación se describe las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se está desarrollando una aplicación +WEB+ en +ASP.NET+.
. El sistema no debe almacenar información sensible
en archivos temporales o en memoria caché.<<r1,^[1]^>>

== Solución

La memoria caché es una memoria auxiliar
de acceso rápido, la cual se encarga
de almacenar de manera temporal
los datos recientes y más frecuentados del usuario.
Cuando se accede por primera vez a un dato,
se guarda una copia de éste dentro de la memoria caché
de forma que el siguiente acceso se realice a dicha copia,
reduciendo el tiempo de acceso
y mejorando el rendimiento.

El caché web, es un tipo especial de caché,
que tiene por objetivo reducir el ancho de banda consumido,
la carga de los servidores y el retraso de las descargas <<r2,^[2]^>>.
Dentro de esta categoría se distinguen 3 tipos de caché web:

* *Privados:* Funciona para un usuario.
* *Compartidos:* Funcionan para varios usuarios.
* *Pasarela:* Funcionan para un servidor.

En el ámbito del desarrollo web,
es frecuente que las aplicaciones puedan aumentar el rendimiento
si se almacenan en la memoria caché
los datos a los que se tiene acceso de forma frecuente
y cuya creación requiere un tiempo de procesamiento significativo <<r3,^[3]^>>.
Uno de los factores más críticos
a la hora de crear aplicaciones Web,
que sean escalables y de alto rendimiento
es la capacidad para almacenar elementos en memoria,
ya sean objetos de datos, páginas
o secciones de páginas en el momento
en el que son solicitados <<r4,^[4]^>>.
Sin embargo, para mejorar la confidencialidad,
es importante limitar ciertas páginas de la aplicación
de ser almacenadas en caché por el navegador web.
Esto evita que un atacante
con acceso a la máquina de un usuario legítimo
pueda obtener información de las páginas visitadas.

. Para prevenir el almacenamiento de páginas en caché
se modifica el +Page_Load+<<r5,^[5]^>> de la página en cuestión.
Agregando cualquiera de las siguientes líneas:
+
.test.py
[source, java, linenums]
----
Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
Response.Cache.SetCacheability(HttpCacheability.NoCache);
----

. Cada vez que se haga clic en un botón
hacia delante o hacia atrás del explorador,
o cualquier evento que genere un +POSTBACK+,
se solicitará al servidor una nueva versión de la página.

== Referencias

. [[r1]] link:../../../products/rules/list/177/[REQ.177 Almacenar datos de forma segura].
. [[r2]] link:https://es.wikipedia.org/wiki/Cach%C3%A9_(inform%C3%A1tica)[Memoria Caché].
. [[r3]] link:https://msdn.microsoft.com/es-es/library/xsbfdd8c(v=vs.100).aspx[Almacenamiento en caché en ASP.NET].
. [[r4]] link:https://msdn.microsoft.com/es-es/library/ms178597(v=vs.100).aspx[Información general sobre el almacenamiento en caché en ASP.NET].
. [[r5]] link:http://www.esasp.net/2010/06/como-evitar-cache-de-pagina-en-el.html[Como evitar Cache de Página en el Explorador].
