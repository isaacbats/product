:slug: products/defends/aspnet/limitar-tiempo-sesion/
:category: aspnet
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la configuracion segura del tiempo de sesión en ASP.NET modificando de forma personalizada en tiempo de expiración de la sesión de los usuarios según requiera el caso.
:keywords: ASP.NET, Seguridad, Limitar, Tiempo, Sesión, Buenas prácticas.
:defends: yes

= Limitar tiempo de sesión

== Necesidad

Se requiere controlar el tiempo de sesión para
una aplicación desarrollada en +ASP.NET+.

== Contexto

A continuación se describe las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se desarrolla una aplicación en +ASP.NET+.
. El sistema debe cerrar una sesión
si se presenta un tiempo de inactividad por parte del usuario.<<r1, ^[1]^>>

== Solución

Cuando se desarrollan aplicaciones web,
muchas veces es necesario modificar variables
a través de distintas páginas.
Para ello es posible utilizar métodos
tales como +GET+ y +POST+.
Sin embargo, también es posible pasar parámetros o variables
utilizando las variables de sesión.
Una sesión es una variable creada dentro del servidor,
la cual puede ejecutarse sin que el usuario tenga conocimiento de ello.
Una variable de sesión tiene la ventaja
de que puede ser accesible en cualquier parte del sitio Web
sin que haya la necesidad de crear enlaces
de pasos de variable o formularios con métodos +GET+ o +POST+,
de tal forma que el usuario no tendrá conocimiento
de cómo ni cuándo se crea,
ni dónde o cuándo se accede y ejecuta la variable.
Cuando el usuario cambia de una página a otra
mediante un enlace normal,
tendrá la variable disponible para usarla.
Sin la presencia de variables de sesión dentro de una aplicación web
no sería posible implementar sistemas seguros de identificación.<<r2, ^[2]^>>

Las variables de sesión poseen un tiempo de caducidad,
el cual se establece de forma predeterminada en el servidor.
La sesión se elimina pasados 20 minutos de inactividad del usuario,
a este periodo de tiempo
antes de la eliminación de la sesión en el servidor
se conoce como tiempo de espera de sesión.
El tiempo de espera de sesión define el tiempo
en el que un usuario puede mantenerse inactivo
antes de que termine la sesión
o de que el usuario tenga que iniciar sesión de nuevo.
El tiempo de espera de sesión también incluye
a los administradores que inician sesión
a través del servicio del portal.<<r3 , ^[3]^>>

Para controlar el tiempo de sesión
de las aplicaciones desarrolladas en +ASP.NET+,
se debe configurar el valor para el atributo +timeout+
en el subelemento +< sessionState>+ del elemento +< system.web>+
dentro del archivo de configuración +web.config+.<<r4, ^[4]^>>
El archivo debe tener las siguientes líneas:

.Web.config
[source,xml,linenums]
----
<?xml version="1.0" encoding="utf-8" ?>
<configuration>
    <system.web>
         <sessionState timeout="# de minutos" />
    </system.web>
</configuration
----

* *NOTA:* El valor establecido en el atributo +timeout+
 representa los *minutos* antes de que la sesión expire.
 El valor máximo que se puede configurar es de 525.601 minutos (1 año).


== Referencias

. [[r1]] link:../../../products/rules/list/023/[REQ.023 Cerrar sesión de un usuario inactivo].

. [[r2]] link:http://www.uterra.com/codigo_php/codigo_php.php?ref=las_variables_de_sesion_en_php[Variables de sesión].

. [[r3]] link:https://www.ibm.com/support/knowledgecenter/es/SS3NGB_1.6.0/ioc/ba_install_session_timeout.html[Tiempo de espera de sesión].

. [[r4]] link:https://msdn.microsoft.com/es-es/library/h6bb9cz9(VS.80).aspx[Elemento sessionState (Esquema de configuración de ASP.NET)].
