:slug: products/defends/php/generar-cookies-httponly/
:category: php
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la programacion segura en PHP al generar cookies con HTTPOnly. Las cookies deben ser aseguradas debido a que contienen información que puede ser usada por un atacante para suplantar la identidad de un usuario.
:keywords: PHP, Servidor, Generar, Cookies, HTTPOnly, Seguridad.
:defends: yes

= Generar Cookies con HttpOnly

== Necesidad

Cookies HttpOnly en PHP.

== Contexto

A continuación se describen las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se tiene acceso al servidor.
. Se desea proteger las +cookies+ con +HttpOnly+.<<r1, ^[1]^>>

== Solución

Una +cookie+ o galleta es una pequeña porción de información
enviada por un sitio o servidor web
y almacenada en el navegador del usuario.<<r2, ^[2]^>>
Las principales funciones o utilidades de las +cookies+ son:

* *Gestión de Sesiones:* de esta manera es posible diferenciar
los usuarios que acceden a una aplicación web,
permitíendole al servidor llevar un control de los mismos
y en consecuencia actuar de diferente forma dependiendo de estos.

* *Personalización:* con las +cookies+ es posible almacenar
la información sobre los hábitos de navegación del usuario,
preferencias, temas y otras configuraciones.
Esto con el fin de ofrecer una funcionalidad
acorde a los gustos específicos de cada usuario.

* *Rastreo:* esto se hace para guardar
y analizar el comportamiento del usuario,
de tal manera que sea posible crear y mantener estadísticas de uso
o también para crear perfiles de usuarios
que luego se usarán para orientar campañas publicitarias.

Para la primera función, gestión de sesiones,
se crean las +cookies+ de sesión
al momento en que un usuario inicia
o realiza el proceso de +login+ ante una aplicación web,
de igual manera, cuando el usuario cierra
o destruye la sesión,
las +cookies+ creadas se eliminan.

No obstante, las +cookies+ se pueden utilizar
para realizar ataques contra la seguridad
del sitio web y de los usuarios que lo frecuentan.

El principal ataque que aprovecha
el uso de las +cookies+ es el robo de las mismas,
es decir, obtener el valor de la +cookie+ de sesión
de otro usuario para suplantarlo
e ingresar a la aplicación web como él.

La manera más común de lograr dicho robo
es mediante un ataque +XSS+.
Dicho ataque permite a una tercera persona
inyectar en páginas web visitadas por el usuario
código +JavaScript+ u otro lenguaje similar.

Entonces, para evitar el robo de +cookies+ mediante este ataque,
se recomienda establecer el atributo +HttpOnly+
a las +cookies+ de sesión.
Este atributo lo que hace es indicarle al navegador
que una +cookie+ no puede ser consultada nunca por medio de +JavaScript+
y que sólo debe usarse cuando se vaya a utilizar en una petición +HTTP+.

. Para +cookies+ de sesión manejadas por +PHP+,
la opción se puede habilitar
en el archivo +php.ini+ a través del parámetro:
+
.test.py
[source, shell, linenums]
----
session.cookie_httponly = True
----

. También se puede habilitar en un +script+
a través de la función:<<r3, ^[3]^>>
+
[source, php, linenums]
----
void session_set_cookie_params ( int $lifetime [, string $path [, string $domain
  [, bool $secure= false [, bool $httponly= true ]]]] )
----

. Para +cookies+ de aplicación,
el último parámetro en +setcookie()+ habilita la opción:
+
[source, php, linenums]
----
bool setcookie ( string $name [, string $value [, int $expire= 0 [, string $path
  [,string $domain [, bool $secure= false [, bool $httponly= true ]]]]]] )
----

== Referencias

. [[r1]] link:../../../products/rules/list/029/[REQ.029 Cookies con atributos de seguridad].
. [[r2]] link:https://developer.mozilla.org/es/docs/Web/HTTP/Cookies[HTTP cookies].
. [[r3]] link:https://www.owasp.org/index.php/HttpOnly#Using_PHP_to_set_HttpOnly[Using PHP to set HttpOnly].
