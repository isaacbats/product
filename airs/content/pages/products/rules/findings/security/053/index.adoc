:slug: products/rules/findings/053/
:description: This finding presents information about vulnerabilities enabling brute force attacks.
:keywords: Brute, Force, Automated, Attack, Protection, Credentials
:findings: yes
:type: security

= F053. Lack of protection against brute force attacks

== Description

The system does not have appropriate protection mechanisms against automated
attacks for guessing credentials.

== Rules

. [[r1]] [inner]#link:/products/rules/list/237/[R237. Ascertain human interaction]#

. [[r3]] [inner]#link:/products/rules/list/327/[R327. Set rate limit]#
