:slug: products/rules/findings/119/
:description: The purpose of this page is to present information about the set of findings reported by Fluid Attacks. In this case, the finding presents information about vulnerabilities arising from exposing sensitive information through metadata, recommendations to avoid them and related security requirements.
:keywords: Metadata, Sensitive, Information, Exposed, Public, File
:findings: yes
:type: hygiene

= F119. Metadata with sensitive information

== Description

The system exposes sensitive information through public files metadata.

== Rules

. [[r1]] [inner]#link:/products/rules/list/045/[R045. Remove metadata when sharing files]#
