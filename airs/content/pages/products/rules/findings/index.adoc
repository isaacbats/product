:slug: products/rules/findings/
:description: The purpose of this page is to present a set of findings reported by Fluid Attacks. Findings is a standardization of the set of types of vulnerabilities that serve as a basis for the security analysis performed by Fluid Attacks. This is an ever-evolving effort as new types arise every day.
:keywords: Fluid Attacks, Products, Findings, Vulnerabilities, Security, Applications.
:findingsindex: yes
:template: findings

= Findings
