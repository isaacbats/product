:slug: products/rules/list/062/
:category: architecture
:description: This requirement establishes the importance of using standard industry-approved configurations.
:keywords: Configuration, Vulnerability, Standard, Industry, ASVS, CAPEC, GDPR, PCI DSS, Rules, Ethical Hacking, Pentesting
:rules: yes

= R062. Define standard configurations

== Requirement

The organization must define standard configurations that correct all known
vulnerabilities.
These configurations must also be consistent with industry standards.

== Description

System configuration is essential when it comes to security issues.
The system must follow the industry's standard configurations that prevent
all known vulnerabilities.
These settings also contribute to ensuring the ongoing confidentiality,
integrity, availability and resilience of systems and services.

== Findings

* [inner]#link:/products/rules/findings/043/[F043. Improperly set HTTP headers]#

* [inner]#link:/products/rules/findings/077/[F077. ARP spoofing]#

* [inner]#link:/products/rules/findings/084/[F084. MDNS spoofing]#

* [inner]#link:/products/rules/findings/110/[F110. HTTP request smuggling]#

* [inner]#link:/products/rules/findings/111/[F111. Out-of-bounds Read]#

* [inner]#link:/products/rules/findings/115/[F115. Security controls bypass]#

* [inner]#link:/products/rules/findings/116/[F116. XS-Leaks]#

== References

. [[r1]] link:https://www.bsimm.com/framework/intelligence/standards-and-requirements.html[BSIMM9 SR3.3: 9. Use secure coding standards].
Secure coding standards help the organization’s developers avoid the most
obvious bugs and provide ground rules for code review.

. [[r2]] link:http://capec.mitre.org/data/definitions/125.html[CAPEC-125: Flooding].
An adversary consumes the resources of a target by rapidly engaging in a large
number of interactions with the target.
This type of attack generally exposes a weakness in rate limiting or flow.
When successful this attack prevents legitimate users from accessing the
service and can cause the target to crash.

. [[r3]] link:http://capec.mitre.org/data/definitions/130.html[CAPEC-130: Excessive Allocation].
An adversary causes the target to allocate excessive resources to servicing the
attackers' request,
thereby reducing the resources available for legitimate services and degrading
or denying services.
Usually, this attack focuses on memory allocation,
but any finite resource on the target could be the attacked, including
bandwidth, processing cycles, or other resources.

. [[r4]] link:http://capec.mitre.org/data/definitions/151.html[CAPEC-151: Identity Spoofing].
Identity Spoofing refers to the action of assuming (i.e., taking on) the
identity of some other entity (human or non-human) and then using that identity
to accomplish a goal.
An adversary may craft messages that appear to come from a different principle
or use stolen / spoofed authentication credentials.
Alternatively, an adversary may intercept a message from a legitimate sender
and attempt to make it look like the message comes from them without changing
its content.

. [[r5]] link:http://capec.mitre.org/data/definitions/161.html[CAPEC-161: Infrastructure Manipulation].
An attacker exploits characteristics of the infrastructure of a network entity
in order to perpetrate attacks or information gathering on network objects or
effect a change in the ordinary information flow between network objects.

. [[r6]] link:https://www.cisecurity.org/controls/[CIS Controls. 5.1 Establish Secure Configurations].
Maintain documented security configuration standards for all authorized
operating systems and software.

. [[r7]] link:https://www.cisecurity.org/controls/[CIS Controls. 11.1 Maintain Standard Security Configurations for Network Devices].
Maintain documented security configuration standards for all authorized network
devices.

. [[r8]] link:https://www.cisecurity.org/controls/[CIS Controls. 18.10 Deploy Web Application Firewalls].
Protect web applications by deploying web application firewalls (*WAFs*) that
inspect all traffic flowing to the web application for common web application
attacks.

. [[r9]] link:https://gdpr-info.eu/art-32-gdpr/[GDPR. Art. 32: Security of processing.(1)(b).]
The controller and the processor shall implement appropriate technical and
organizational measures to ensure an appropriate level of security,
including the ability to ensure the ongoing confidentiality, integrity,
availability and resilience of processing systems and services.

. [[r10]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A4-XML_External_Entities_(XXE)[OWASP Top 10 A4:2017-XML External Entities (XXE)].
Many older or poorly configured *XML* processors evaluate external entity
references within *XML* documents.
External entities can be used to disclose internal files using the file *URI*
handler, internal file shares, internal port scanning, remote code execution,
and denial of service attacks.

. [[r11]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A6-Security_Misconfiguration[OWASP Top 10 A6:2017-Security Misconfiguration].
Security misconfiguration is the most commonly seen issue.
This is commonly a result of insecure default configurations,
incomplete or ad hoc configurations, open cloud storage,
misconfigured *HTTP* headers,
and verbose error messages containing sensitive information.
Not only must all operating systems, frameworks, libraries, and applications be
securely configured, but they must be patched/upgraded in a timely fashion.

. [[r12]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
Appendix C: Internet of Things Verification Requirements.(C.17)]
Verify that any available Intellectual Property protection technologies
provided by the chip manufacturer are enabled.

. [[r13]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
Appendix C: Internet of Things Verification Requirements.(C.32)]
Verify that the firmware apps utilize kernel containers for isolation between
apps.

. [[r14]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
Appendix C: Internet of Things Verification Requirements.(C.33)]
Verify that secure compiler flags such as **-fPIE**, **-fstack-protector-all**,
**-Wl**, **-z,noexecstack**, **-Wl**, **-z,noexecheap** are configured for
firmware builds.

. [[r15]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
Appendix C: Internet of Things Verification Requirements.(C.34)]
Verify that micro controllers are configured with code protection
(if applicable).

. [[r16]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.5 Deserialization Prevention Requirements.(5.5.2)]
Verify that the application correctly restricts *XML* parsers to only use the
most restrictive configuration possible and to ensure that unsafe features such
as resolving external entities are disabled to prevent *XXE*.

. [[r17]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V6.2 Algorithms.(6.2.2)]
Verify that industry proven or government approved cryptographic algorithms,
modes, and libraries are used, instead of custom coded cryptography.

. [[r18]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V6.2 Algorithms.(6.2.3)]
Verify that encryption initialization vector, cipher configuration,
and block modes are configured securely using the latest advice.

. [[r19]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V9.2 Server Communications Security Requirements.(9.2.4)]
Verify that proper certification revocation, such as Online Certificate Status
Protocol (**OCSP**) Stapling, is enabled and configured.

. [[r20]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V10.3 Deployed Application Integrity Controls.(10.3.2)]
Verify that the application employs integrity protections,
such as code signing or sub-resource integrity.
The application must not load or execute code from untrusted sources,
such as loading includes, modules, plugins, code, or libraries from untrusted
sources or the Internet.

. [[r21]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V13.2 RESTful Web Service Verification Requirements.(13.2.2)]
Verify that *JSON* schema validation is in place and verified before accepting
input.

. [[r22]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V13.3 SOAP Web Service Verification Requirements.(13.3.1)]
Verify that *XSD* schema validation takes place to ensure a properly formed
*XML* document,
followed by validation of each input field before any processing of that data
takes place.

. [[r23]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V14.1 Build.(14.1.1)]
Verify that the application build and deployment processes are performed in a
secure and repeatable way, such as *CI* / *CD* automation,
automated configuration management, and automated deployment scripts.

. [[r24]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V14.1 Build.(14.1.2)]
Verify that compiler flags are configured to enable all available buffer
overflow protections and warnings,
including stack randomization, data execution prevention,
and to break the build if an unsafe pointer, memory, format string, integer,
or string operations are found.

. [[r25]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V14.1 Build.(14.1.3)]
Verify that server configuration is hardened as per the recommendations of the
application server and frameworks in use.

. [[r26]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.2.2]
Secure and synchronize router configuration files.

. [[r27]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.3.4]
Implement anti-spoofing measures to detect and block forged source IP addresses
from entering the network.

. [[r28]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.3.5]
Permit only "established" connections into the network.

. [[r29]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 2.2.3]
Implement additional security features for any required services, protocols,
or daemons that are considered to be insecure.

. [[r30]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 2.2.4]
Configure system security parameters to prevent misuse.

. [[r31]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 6.2]
Ensure that all system components and software are protected from known
vulnerabilities by installing applicable vendor-supplied security patches.

. [[r32]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 6.3]
Develop internal and external software applications
(including web-based administrative access to applications)
securely.
Based on industry standards and/or best practices.
