:slug: products/rules/list/033/
:category: authorization
:description: This requirement establishes the importance of limiting administrative access to apps to authorized users only in order to avoid several common attacks.
:keywords: Restrict, Administrative, Access Control, Network, CWE, ASVS, OWASP, PCI DSS, Rules, Ethical Hacking, Pentesting
:rules: yes

= R033. Restrict administrative access

== Requirement

If the system has an administration mechanism,
it must only be accessible from administrative network segments.

== Description

Network access to modules or system management mechanisms
must be limited to the parties
that require access to them (administrators).
Personnel that does not have administrative needs, tasks or obligations
should not have access to these mechanisms.
Following this recommendation helps to fulfill the objective
of reducing the attack surface of the above mentioned systems
(since malicious third parties cannot attempt
to directly access the system administration settings),
and increases the level of confidentiality and availability of the system.

== Implementation

. Principle of least privilege:
For each system in the organization
it must be guaranteed that each module
(process, user or program) can only access
the information and resources required
to accomplish its legitimate purpose.

== Attacks

. An anonymous attacker attempts to access an exposed administrator
interface by brute force,
which may cause a denial of service, account lockouts or an
interface/system lockout.

. An anonymous attacker and/or registered user
exploits a known vulnerability in the management system,
which may allow access to the system settings,
a denial of service or privileges elevation for system users or processes.

. An anonymous attacker obtains technical information of the system
through data analysis of the administrator interface
in order to perform deeper and more detailed attacks.

== Attributes

* Layer: Application layer
* Asset: System management
* Scope: Confidentiality
* Phase: Operation
* Type of control: Recommendation

== Findings

* [inner]#link:/products/rules/findings/054/[F054. Exposed administrative services]#

== References

. [[r1]] link:https://cwe.mitre.org/data/definitions/419.html[CWE-419: Unprotected Primary Channel].
The software uses a primary channel for administration or restricted
functionality,
but it does not properly protect the channel.

. [[r2]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A5-Broken_Access_Control[OWASP Top 10 A5:2017-Broken Access Control].
Restrictions on what authenticated users are allowed to do are often not
properly enforced.
Attackers can exploit these flaws to access unauthorized functionality and/or
data, such as access other users' accounts, view sensitive files,
modify other users' data, change access rights, etc.

. [[r3]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V13.1 Generic Web Service Security Verification Requirements.(13.1.2)]
Verify that access to administration and management functions is limited to
authorized administrators.

. [[r4]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.2.1]
Restrict inbound and outbound traffic to that which is necessary for the
cardholder data environment,
and specifically deny all other traffic.

. [[r5]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.2.2]
Secure and synchronize router configuration files.

. [[r6]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.3.6]
Place system components that store cardholder data (such as a database) in an
internal network zone,
segregated from the *DMZ* and other untrusted networks.

. [[r7]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 6.5.8]
Address common coding vulnerabilities in software-development processes
including improper access control
(such as insecure direct object references, failure to restrict URL access,
directory traversal, and failure to restrict user access to functions).

. [[r8]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 8.7]
Restrict access to any database so that only database administrators have the
ability to directly access or query databases.
