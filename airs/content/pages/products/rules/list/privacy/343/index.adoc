:slug: products/rules/list/343/
:category: privacy
:description: This requirement establishes the importance of respecting the users' tracking preference indicated in the Do Not Track (DNT) header.
:keywords: Track, Preference, Header, Data, Privacy, GDPR, ISO, Rules, Ethical Hacking, Pentesting
:rules: yes

= R343. Respect the Do Not Track header

== Requirement

The system must respect the user's chosen value for the Do Not Track (*DNT*)
header.

== Description

Systems usually request information from their users,
obtain it from third parties
or collect it based on their interactions with the application.
The *DNT* header holds information about a user's preference regarding being
tracked by the websites they visit.
Websites often ignore this header and proceed to track the user's activities,
which could represent a privacy violation if no consent is requested.

== Findings

* [inner]#link:/products/rules/findings/088/[F088. Privacy violation]#

== References

. [[r1]] link:https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:02002L0058-20091219[Directive 2002/58/EC (amended by E-privacy Directive 2009/136/EC).
Art. 6: Traffic data.(4)]
The service provider must inform the subscriber or user of the types of traffic
data which are processed and of the duration of such processing for the
purposes mentioned in paragraph 2 and,
prior to obtaining consent,
for the purposes mentioned in paragraph 3.

. [[r2]] link:https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:02002L0058-20091219[Directive 2002/58/EC (amended by E-privacy Directive 2009/136/EC).
Art. 9: Location data other than traffic data.(1)]
The service provider must inform the users or subscribers,
prior to obtaining their consent,
of the type of location data other than traffic data which will be processed,
of the purposes and duration of the processing
and whether the data will be transmitted to a third party for the purpose of
providing the value added service.

. [[r3]] link:https://gdpr-info.eu/recitals/no-39/[GDPR. Recital 39: Principles of data processing].
It should be transparent to natural persons that personal data concerning them
are collected, used, consulted or otherwise processed and to what extent
the personal data are or will be processed.

. [[r4]] link:https://gdpr-info.eu/recitals/no-40/[GDPR. Recital 40: Lawfulness of data processing].
In order for processing to be lawful,
personal data should be processed on the basis of the consent of the data
subject concerned or some other legitimate basis.

. [[r5]] link:https://www.iso.org/obp/ui/#iso:std:54534:en[ISO 27001:2013. Annex A - 18.1.4]
When applicable, guarantee the privacy and security of personal information,
as required by the relevant legislation and regulations.
