:slug: products/rules/list/257/
:category: networks
:description: This requirement establishes the importance of defining an access model based on organizational user credentials.
:keywords: Requirement, Security, Physical, Access, Network, Credentials, HIPAA, ISO, NIST, Rules, Ethical Hacking, Pentesting
:rules: yes

= R257. Access based on user credentials

== Requirement

Physical access to the network for users
must be assigned based on organizational user credentials
(e.g., **NAC 802.1x**).

== References

. [[r1]] link:https://www.cisecurity.org/controls/[CIS Controls. 1.7 Deploy Port Level Access Control].
Utilize port level access control, following 802.1x standards, to control which
devices can authenticate to the network.

. [[r2]] link:https://www.cisecurity.org/controls/[CIS Controls. 15.8 Use Wireless Authentication Protocols That Require Mutual,
Multi-Factor Authentication].
Ensure that wireless networks use authentication protocols such as Extensible
Authentication Protocol-Transport Layer Security (**EAP/TLS**),
that requires mutual, multi-factor authentication.

. [[r3]] link:https://www.law.cornell.edu/cfr/text/45/164.312[HIPAA Security Rules 164.312(e)(1):]
Transmission Security: Implement technical security measures
to guard against unauthorized access
to electronic protected health information
that is being transmitted over an electronic communications network.

. [[r4]] link:https://www.iso.org/obp/ui/#iso:std:54534:en[ISO 27001:2013. Annex A - 9.1.2]
Users should only have access to the internal network and network
services for which they have been explicitly authorized.

. [[r5]] link:https://nvd.nist.gov/800-53/Rev4/control/IA-2[NIST 800-53 IA-2]
Identification and authentication:
The information system uniquely identifies and authenticates
organizational users (or processes acting on behalf of organizational users).
