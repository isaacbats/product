:slug: products/rules/list/259/
:category: networks
:description: This requirement establishes the importance of separating logical networks by segmenting them for different functional areas.
:keywords: Requirement, Security, Logical, Network, Segment, Areas, ASVS, CWE, ISO, OWASP, PCI DSS, Rules, Ethical Hacking, Pentesting
:rules: yes

= R259. Segment the organization network

== Requirement

The organization network must be segmented.

== References

. [[r1]] link:https://www.cisecurity.org/controls/[CIS Controls. 12.3 Deny Communications With Known Malicious IP Addresses].
Deny communications with known malicious or unused Internet IP addresses and
limit access only to trusted and necessary IP address ranges at each of the
organization’s network boundaries.

. [[r2]] link:https://www.cisecurity.org/controls/[CIS Controls. 14.1 Segment the Network Based on Sensitivity].
Segment the network based on the label or classification level of the
information stored on the servers,
locate all sensitive information on separated Virtual Local Area Networks
(*VLANs*).

. [[r3]] link:https://cwe.mitre.org/data/definitions/923.html[CWE-923: Improper Restriction of Communication Channel to Intended Endpoints].
The software establishes a communication channel to an endpoint for
privileged or protected operations,
but it does not properly ensure that it is communicating with the correct
endpoint.

. [[r4]] link:https://www.iso.org/obp/ui/#iso:std:54534:en[ISO 27001:2013. Annex A - 13.1.3]
Separate information systems, users and information services groups in the
network.

. [[r5]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A6-Security_Misconfiguration[OWASP Top 10 A6:2017-Security Misconfiguration].
Security misconfiguration is the most commonly seen issue.
This is commonly a result of insecure default configurations,
incomplete or ad hoc configurations, open cloud storage,
misconfigured *HTTP* headers,
and verbose error messages.

. [[r6]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V6.4 Secret Management.(6.4.1)]
Verify the segregation of components of differing trust levels through
well-defined security controls, firewall rules, *API* gateways,
reverse proxies, cloud-based security groups, or similar mechanisms.

. [[r7]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.2.1]
Restrict inbound and outbound traffic to that which is necessary for the
cardholder data environment,
and specifically deny all other traffic.

. [[r8]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.2.3]
Install perimeter firewalls between all wireless networks and the cardholder
data environment.
Configure these firewalls to deny or,
if traffic is necessary for business purposes,
permit only authorized traffic.

. [[r9]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.3.1]
Implement a *DMZ* to limit inbound traffic to only system components that
provide authorized publicly accessible services, protocols, and ports.

. [[r10]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.3.2]
Limit inbound Internet traffic to *IP* addresses within the *DMZ*.

. [[r11]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.3.3]
Implement anti-spoofing measures to detect and block forged source IP addresses
from entering the network.

. [[r12]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.3.4]
Do not allow unauthorized outbound traffic from the cardholder data environment
to the Internet.

. [[r13]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.3.6]
Place system components that store cardholder data (such as a database) in an
internal network zone,
segregated from the *DMZ* and other untrusted networks.
