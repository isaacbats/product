:slug: products/rules/list/258/
:category: networks
:description: This requirement establishes the importance of restricting the content of certain websites using a custom proxy configuration.
:keywords: Security, Requirement, Content, Filter, Proxy, Network, Rules, Ethical Hacking, Pentesting
:rules: yes

= R258. Filter website content

== Requirement

The organization must filter the content of websites
accessed from a location belonging to the same entity
(Output 'Proxy').

== References

. [[r1]] link:https://www.cisecurity.org/controls/[CIS Controls. 7.7 Use of DNS Filtering Services].
Use Domain Name System (DNS) filtering services to help block access to known
malicious domains.
