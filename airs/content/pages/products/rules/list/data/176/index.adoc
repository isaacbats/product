:slug: products/rules/list/176/
:category: data
:description: This requirement establishes the importance of restricting access to sensitive information to authorized users only.
:keywords: Data, Authorization, Restriction, GDPR, ASVS, CWE, NERC, OWASP, PCI DSS, Rules, Ethical Hacking, Pentesting
:rules: yes

= R176. Restrict system objects

== Requirement

The system must restrict access to system objects
that have sensitive content.
It should only allow access to authorized users.

== Description

Applications usually handle personal and confidential information,
such as personal identifications, social security numbers,
credentials and health histories.
This data should be protected as a fundamental right,
and therefore be stored and transmitted using secure mechanisms that
prevent access to it by unauthorized actors.
Furthermore, the access control model and role assignment policy
must be implemented taking these restrictions into consideration.

== Findings

* [inner]#link:/products/rules/findings/013/[F013. Insecure object reference]#

* [inner]#link:/products/rules/findings/032/[F032. Spoofing]#

* [inner]#link:/products/rules/findings/037/[F037. Technical information leak]#

* [inner]#link:/products/rules/findings/038/[F038. Business information leak]#

* [inner]#link:/products/rules/findings/039/[F039. Improper authorization control for web services]#

* [inner]#link:/products/rules/findings/040/[F040. Exposed web services]#

* [inner]#link:/products/rules/findings/075/[F075. Unauthorized access to files]#

* [inner]#link:/products/rules/findings/116/[F116. XS-Leaks]#

== References

. [[r1]] link:https://www.cisecurity.org/controls/[CIS Controls. 14.6 Protect Information Through Access Control Lists].
Protect all information stored on systems with file system, network share,
claims, application, or database specific access control lists.

. [[r2]] link:https://cwe.mitre.org/data/definitions/284.html[CWE-284: Improper Access Control].
The software does not restrict or incorrectly restricts access to a resource
from an unauthorized actor.

. [[r3]] link:https://cwe.mitre.org/data/definitions/287.html[CWE-287: Improper Authentication].
When an actor claims to have a given identity,
the software does not prove or insufficiently proves that the claim is correct.

. [[r4]] link:https://cwe.mitre.org/data/definitions/306.html[CWE-306: Missing Authentication for Critical Function].
The software does not perform any authentication for functionality that
requires a provable user identity or consumes a significant amount of
resources.

. [[r5]] link:https://cwe.mitre.org/data/definitions/359.html[CWE-359: Exposure of Private Personal Information to an Unauthorized Actor].
The product does not properly prevent a person's private, personal information
from being accessed by actors who either are not explicitly authorized to
access the information or do not have the implicit consent of the person about
whom the information is collected.

. [[r6]] link:https://cwe.mitre.org/data/definitions/639.html[CWE-639: Authorization Bypass Through User-Controlled Key].
The system's authorization functionality does not prevent one user from gaining
access to another user's data or record by modifying the key value identifying
the data.

. [[r7]] link:https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:02002L0058-20091219[Directive 2002/58/EC (amended by E-privacy Directive 2009/136/EC).
Art. 4: Security of processing.(1a)]
The measures referred to in paragraph 1 shall at least ensure that personal
data can be accessed only by authorized personnel for legally authorized
purposes.

. [[r8]] link:https://gdpr-info.eu/art-32-gdpr/[GDPR. Art. 32: Security of processing.(4)]
The controller and processor shall take steps to ensure that any natural person
acting under the authority of the controller or the processor who has access to
personal data does not process them except on instructions from the controller.

. [[r9]] link:https://gdpr-info.eu/recitals/no-2/[GDPR. Recital 6: Ensuring a High Level of Data Protection Despite
the Increased Exchange of Data].
Technology has transformed both the economy and social life,
and should further facilitate the free flow of personal data,
while ensuring a high level of the protection of personal data.

. [[r10]] link:https://www.nerc.com/pa/Stand/Reliability%20Standards/CIP-003-8.pdf[NERC CIP-003-8. Attachment 1. Section 3 - 3.1]
Permit only necessary inbound and outbound electronic access as determined by
the Responsible Entity.

. [[r11]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A3-Sensitive_Data_Exposure[OWASP Top 10 A3:2017-Sensitive Data Exposure].
Many web applications and **API**s do not properly protect sensitive data,
such as financial, healthcare, and *PII*.
Attackers may steal or modify such weakly protected data to conduct credit card
fraud, identity theft, or other crimes.
Sensitive data may be compromised without extra protection,
such as encryption at rest or in transit, and requires special precautions when
exchanged with the browser.

. [[r12]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A5-Broken_Access_Control[OWASP Top 10 A5:2017-Broken Access Control].
Restrictions on what authenticated users are allowed to do are often not
properly enforced.
Attackers can exploit these flaws to access unauthorized functionality and/or
data, such as access other users' accounts, view sensitive files,
modify other users' data, change access rights, etc.

. [[r13]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V1.2 Authentication Architectural Requirements.(1.2.2)]
Verify that communications between application components,
including APIs, middleware and data layers, are authenticated.
Components should have the least necessary privileges needed.

. [[r14]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V1.4 Access Control Architectural Requirements.(1.4.5)]
Verify that attribute or feature-based access control is used whereby the code
checks the user's authorization for a feature/data item rather than just their
role.
Permissions should still be allocated using roles.

. [[r15]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V4.2 Operation Level Access Control.(4.2.1)]
Verify that sensitive data and APIs are protected against direct object attacks
targeting creation, reading, updating and deletion of records,
such as creating or updating someone else's record, viewing everyone's records,
or deleting all records.

. [[r16]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V13.2 RESTful Web Service Verification Requirements.(13.2.1)]
Verify that enabled **RESTful HTTP** methods are a valid choice for the user or
action,
such as preventing normal users using *DELETE* or *PUT* on protected *API* or
resources.

. [[r17]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.2.2]
Secure and synchronize router configuration files.

. [[r18]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 3.6.7]
Fully document and implement all key-management processes and procedures for
cryptographic keys including prevention of unauthorized substitution of
cryptographic keys.

. [[r19]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 6.5.8]
Address common coding vulnerabilities in software-development processes
including improper access control
(such as insecure direct object references, failure to restrict URL access,
directory traversal, and failure to restrict user access to functions).

. [[r20]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 7.1.1]
Define access needs for each role,
including system components and data resources that each role needs to access
for their job function.

. [[r21]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 10.5.1]
Limit viewing of audit trails to those with a job-related need.
