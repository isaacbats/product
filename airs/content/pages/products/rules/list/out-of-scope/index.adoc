:slug: products/rules/list/out-of-scope/
:description: Out of Scope Security Rules is an additional set where you can find business-related and non-technical rules to verify only by documental and process audits.
:keywords: Fluid Attacks, Products, Rules, Criteria, Security, Applications, Ethical Hacking, Pentesting
:rulesindex: yes
:template: findings

= Out of Scope Security Rules

These security rules can't be verified through a technical process,
only through a Documental and Process audits.
