:slug: products/rules/list/201/
:category: physical
:description: This requirement establishes the importance of defining mechanisms to detect tampering or unauthorized access to physical devices.
:keywords: Requirement, Security, Mobile, Devices, Tampering, Data, Rules, Ethical Hacking, Pentesting
:rules: yes
:extended: yes

= R201. Detect device tampering

== Requirement

Devices must have mechanisms to detect tampering
and/or unauthorized accesses.
