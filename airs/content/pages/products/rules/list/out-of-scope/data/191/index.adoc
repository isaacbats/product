:slug: products/rules/list/191/
:category: data
:description: This requirement establishes the importance of protecting sensitive data with the maximum security level defined in the system.
:keywords: Requirement, Security, System, Protection, Personal Data, User, Rules, Ethical Hacking, Pentesting
:rules: yes
:extended: yes

= R191. Protect data with maximum level

== Requirement

Personal data must be secured with
the maximum protection level defined (+ISACA.G31.5+).

== Description

Applications usually request or collect personal data from their users.
This information must be protected with the maximum level of security,
not only to comply with regulations but also to promote confidence in the
system.

== References

. [[r1]] link:https://gdpr-info.eu/art-25-gdpr/[GDPR. Art. 25: Data protection by design and by default.(1)]
The controller shall,
both at the time of the determination of the means for processing and at the
time of the processing itself,
implement appropriate technical and organizational measures.

. [[r2]] link:https://gdpr-info.eu/recitals/no-6/[GDPR. Recital 6: Ensuring a high level of data protection
despite the increased exchange of data].
Technology has transformed both the economy and social life,
and should further facilitate the free flow of personal data within the Union
and the transfer to third countries and international organizations,
while ensuring a high level of the protection of personal data.

. [[r3]] link:https://gdpr-info.eu/recitals/no-51/[GDPR. Recital 51: Protecting sensitive personal data].
Personal data which are, by their nature, particularly sensitive in relation to
fundamental rights and freedoms merit specific protection as the context of
their processing could create significant risks to the fundamental rights and
freedoms.
