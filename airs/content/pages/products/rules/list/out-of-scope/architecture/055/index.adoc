:slug: products/rules/list/055/
:category: architecture
:description: This requirement establishes the importance of documenting all system security cases in order to facilitate the response to a security breach.
:keywords: Requirement, Security, Cases, Documentation, Record, Procedure, Security Breach, Rules, Ethical Hacking, Pentesting
:rules: yes
:extended: yes

= R055. Document system security cases

== Requirement

All system security cases must be documented.

== References

. [[r1]] link:https://www.law.cornell.edu/cfr/text/45/164.312[+HIPAA Security Rules+ 164.312(a)(2)(ii):]
Emergency Access Procedure: Establish (and implement as needed)
procedures for obtaining necessary electronic protected health information
during an emergency.
