:slug: products/rules/list/079/
:category: logs
:description: This requirement establishes the importance of recording all relevant time parameters to detect the exact moment when a security event occurs.
:keywords: Time, Logs, Events, Occurrence, CWE, ASVS, PCI DSS, Rules, Ethical Hacking, Pentesting
:rules: yes

= R079. Record exact occurrence time of events

== Requirement

The system must log the exact occurrence time
(date, hour, seconds, milliseconds and time zone)
for each exceptional and security event.

== Description

Event logs must contain the exact time of occurrence
in order to allow backtracking in an investigation.

== Implementation

. Once all the events to be logged are defined,
the system must be configured so that these logs
contain the date, hour, seconds, milliseconds and time zone
of the event occurrence.

== Attacks

In a security incident scenario,
event time and duration cannot be clearly identified
due to the lack of detail in log records.

== Attributes

* Layer: Application layer
* Asset: Logs
* Scope: Responsibility
* Phase: Operation
* Type of control: Procedure

== References

. [[r1]] link:https://www.cisecurity.org/controls/[CIS Controls. 6.3 Enable Detailed Logging].
Enable system logging to include detailed information such as an event source,
date, user, timestamp, source addresses, destination addresses, and other
useful elements.

. [[r2]] link:https://cwe.mitre.org/data/definitions/778.html[CWE-778: Insufficient Logging].
When a security-critical event occurs,
the software either does not record the event or omits important details about
the event when logging it.

. [[r3]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A10-Insufficient_Logging%252526Monitoring[OWASP Top 10 A10:2017-Insufficient Logging & Monitoring].
Insufficient logging and monitoring,
coupled with missing or ineffective integration with incident response,
allows attackers to further attack systems, maintain persistence,
pivot to more systems, and tamper, extract, or destroy data.

. [[r4]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V1.7 Errors, Logging and Auditing Architectural Requirements.(1.7.1)]
Verify that a common logging format and approach is used across the system.

. [[r5]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V7.1 Log Content Requirements.(7.1.4)]
Verify that each log event includes necessary information that would allow for
a detailed investigation of the timeline when an event happens.

. [[r6]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V7.3 Log Protection Requirements.(7.3.4)]
Verify that time sources are synchronized to the correct time and time zone.
Strongly consider logging only in UTC if systems are global to assist with
post-incident forensic analysis.

. [[r7]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 10.3]
Record at least the following audit trail entries for all system components for
each event: user identification, type of event, date and time, success or
failure indication, origination of event,
and identity or name of affected data, system component, or resource.
