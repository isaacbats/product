:slug: products/forces/install/
:description: Forces makes use of human skills for the creation of exploits to break your build and force remediation of vulnerabilities.
:keywords: Fluid Attacks, Products, Forces, Ethical Hacking, Pentesting, Security
:forcespage: yes

= Forces Install

You can use forces on any operating system that Python can run on, you can see
the status of the package in link:https://pypi.org/project/forces/[Pypi].
You can also integrate forces into your `CI/CD` to ensure that your software
is built and shipped without previously reported vulnerabilities in
*integrates*.

== Installation

1. Make sure you own an integrates API token. Follow this link:https://community.fluidattacks.com/t/integrates-api-access/540/[guide] to generate it.
2. Make sure your execution environment has the required dependencies:
** git
** Python 3.8
** pip
3. Install forces by running the following command:
** Windows: `python -m pip install forces`.
** Linux and Mac OS: `python3.8 -m pip install -U forces`.
4. You can also make use of the Docker image
`docker pull fluidattacks/forces:new`.
5. Be sure to use forces within a git repository.

== Options

* `--token`: Your token for integrates API [required].
* `--dynamic / --static` Run only DAST / SAST vulnerabilities.
* `-verbose <number>`: Declare the level of detail of the report (default 3)
** `1`:It only shows the number of open, closed and accepted vulnerabilities.
** `2`: Only show open vulnerabilities.
** `3`: Show open and closed vulnerabilities.
** `4`: Show open, closed and accepted vulnerabilities.
** You can use `-v`, `-vv`, `-vvv`, `-vvvv` instead of `--verbose`.
* `--strict / --lax`: Run forces in strict mode (default `--lax`).
* `--repo-path`: Git repository path (optional)

== Examples

In your local environment you execute:
`forces --token <your-token>`.
You can also use the Docker image:
`docker run --rm fluidattacks/forces:new forces --token <your-token>`.

== Use in some CI\CD

in `GitLab`, +
add these three lines to your `.gitlab-ci.yml`:

[source,yaml]
----
forces:
  image:
    name: fluidattacks/forces:new
    entrypoint: [""]
  script:
    - forces --token <your-token> --strict
----

in `Azure DevOps`add these lines to you configuration file:

[source,yaml]
----
jobs:
  - forces:
    container: fluidattacks/forces:new
    steps:
    - bash: forces --token <your-token>
----

in `Jenkins`, the configuration file should look like this:

[source,json]
----
pipeline {
  agent {
    label 'label'
  }
  environment {
    TOKEN = "test"
  }
  stages {
    stage('Forces') {
      steps {
        script {
          sh """
            docker pull fluidattacks/forces:new
            docker run fluidattacks/forces:new --token ${TOKEN}
          """
        }
      }
    }
  }
}
----
