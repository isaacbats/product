# Reviews

[![License](https://img.shields.io/pypi/l/skims)](../LICENSE)

## Installing

Choose one of the methods presented bellow and then run Reviews: `reviews --help`

### Via git

1.  Clone this repository and cd into it.
1.  Run the following command: `./install.sh`

### Via Nix

1.  Install Nix as explained [here](https://nixos.org/download.html).
1.  Run the following command:

    `nix-env -i product -f https://gitlab.com/fluidattacks/product/-/archive/master/product-master.tar.gz`
